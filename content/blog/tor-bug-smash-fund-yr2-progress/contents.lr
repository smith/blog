title: Bug Smash Fund, Year 2: Progress So Far!
---
pub_date: 2021-02-12
---
author: alsmith
---
tags:
fundraising
---
image: /static/images/blog/bug_smash_end.png
---
summary:
Last August, we asked you to help us fundraise during our second annual Bug Smash Fund campaign. We want to share an update on some of the work that the second year of the Bug Smash Fund has made possible.
---
_html_body:
<div class="ace-line" id="magicdomid13"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">Last August, we asked you to help us fundraise during our second annual <a href="https://blog.torproject.org/tor-bug-smash-fund-2020-106K-raised">Bug Smash Fund campaign</a></span><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">. This fund is designed to grow a healthy reserve earmarked for maintenance work, finding bugs, and smashing them—all tasks necessary to keep Tor Browser, the Tor network, and the many tools that rely on Tor strong, safe, and running smoothly.</span><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z b"><b> In 2020, despite the challenges of COVID-19 and event cancellations, you helped us to raise $106,709! </b></span></div>
<div class="ace-line" id="magicdomid14"> </div>
<div class="ace-line" id="magicdomid15"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">We want to share an update on some of the work that the second year of the Bug Smash Fund has made possible.</span></div>
<div class="ace-line"> </div>
<div class="ace-line">
<div class="ace-line" id="magicdomid100"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">Since 2019, we’ve marked 134 tickets with <a href="https://gitlab.torproject.org/groups/tpo/-/issues?scope=all&amp;utf8=%E2%9C%93&amp;state=all&amp;label_name[]=BugSmashFund">BugSmashFund</a>. As of today, 97 of those tickets have been closed, and 37 of them are still in progress. This year, we've used the Bug Smash Fund </span><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z u">to work on</span><span class="author-a-lz82z0z90zm6k8z69zbz78zz83zib9f u"> </span><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z u">continuous integration </span><span class="author-a-lz82z0z90zm6k8z69zbz78zz83zib9f u">tooling</span><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z u">, Tor Browser</span><span class="author-a-60z85zkaoz67zz66zz82zz87zz66zapz69zc7 u"> improvements</span><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z u">, </span><span class="author-a-60z85zkaoz67zz66zz82zz87zz66zapz69zc7 u">defense against DDoS on onion services v3</span><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z u">, GetTor</span><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">, <a href="https://youtu.be/Xpg775CJkaY">Arti</a>, and security fixes. We have also used the Bug Smash Fund to create a new <a href="https://status.torproject.org">status.torproject.org</a> page, which will act as a landing place for network and service status updates.</span></div>
<div class="ace-line" id="magicdomid18"> </div>
<div class="ace-line" id="magicdomid19"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">Thanks for supporting this work!</span></div>
<div class="ace-line" id="magicdomid20"> </div>
<div class="ace-line" id="magicdomid21"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">Below is a list of many of the tickets we’ve closed so far.</span></div>
<div class="ace-line">
<h2 class="ace-line" id="magicdomid23"><span class="author-a-z90z8z78z4z75zz90zd8pz71zz77z5z74zz66zgz79z b"><b>Continuous integration tooling</b></span></h2>
<div class="ace-line" id="magicdomid25"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">When we made the transition from Trac to <a href="https://gitlab.torproject.org">GitLab</a></span><span class="author-a-lz82z0z90zm6k8z69zbz78zz83zib9f"> for issue tracking</span><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">, we moved our CI tooling into GitLab CI and Appveyor. Because this work is critical--but not covered by a grant--the Bug Smash Fund helped us to fix the CI pipeline in GitLab and improve the infrastructure we use to develop Tor. <a href="https://gitlab.torproject.org/tpo/core/tor/-/issues?scope=all&amp;utf8=%E2%9C%93&amp;state=closed&amp;label_name[]=BugSmashFund&amp;label_name[]=CI">See all CI tickets</a></span><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">.</span></div>
<div class="ace-line" id="magicdomid26"> </div>
<div class="ace-line" id="magicdomid27">
<ul class="list-bullet1">
<li><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/40241"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">Fix issue when using FALLTHROUGH with ALL_BUGS_ARE_FATAL #40241</span></a></li>
<li><a href=" https://gitlab.torproject.org/tpo/core/tor/-/issues/40204"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">Travis chutney tests are borked by two bad commits #40204</span></a></li>
<li><a href=" https://gitlab.torproject.org/tpo/core/tor/-/issues/41099"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">Nightly Windows build failures on both 32-bit and 64-bit #40199</span></a></li>
<li><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/40098"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">Parallelize several tests to make hardened-build CI faster. #40098</span></a></li>
<li><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/40091"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">Remove AppVeyor VS2015 build #40091</span></a></li>
<li><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/40076"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">Assertion buf-&gt;tail failed in buf_assert_ok at src/lib/buf/buffers.c:919 #40076</span></a></li>
<li><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/33629"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">Use stale bot to close old pull requests #33629</span></a></li>
<li><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/32943"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">factor out supporting shell scripts from CI configs #32943</span></a></li>
<li><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/32776"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">Remove 0.2.9 from the jenkins builders #32776</span></a></li>
<li><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/32773"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">Remove Jenkins tor master jobs which don't have OpenSSL 1.1.1 #32773</span></a></li>
<li><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/32193"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">update .gitlab-ci.yml to remove broken cruft and add a complete test suite #32193</span></a></li>
<li><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/31921"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">Wrap our Travis commands with travis_retry, to mitigate network timeouts #31921</span></a></li>
<li><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/31862"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">Add a beta RUST_VERSION build to Travis CI #31862</span></a></li>
<li><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/31560"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">Should we CI-build with --disable-module-dirauth and -O0? #31560</span></a></li>
<li><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/30225"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">Run clang's scan-build in Tor's CI #30225</span></a></li>
<li><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/27859"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">update travis CI to ubuntu xenial image when available #27859</span></a></li>
<li><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/40275"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">Debian Hardened CI failures due to lack of ptrace #40275</span></a></li>
<li><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/32817"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">Run sandbox tests on Xenial and Bionic #32817</span></a></li>
<li><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/32722"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">Make the seccomp sandbox work with Ubuntu Xenial and Bionic #32722</span></a></li>
<li><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/31884"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">Define ExecuteBash in the Appveyor error block #31884</span></a></li>
</ul>
<h2 class="ace-line" id="magicdomid48"><span class="author-a-z122zz84zlz71zdvz68z7k1z83z5u0z80zb b"><b>Tor Browser</b></span></h2>
<div class="ace-line" id="magicdomid49"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">The Bug Smash Fund helps us to resolve issues in Tor Browser that are not part of a grant or sponsored project, including fixing AV1 playback. It has also helped us make updates to Tor Browser's custom UI.</span></div>
<div class="ace-line"> </div>
<div class="ace-line" id="magicdomid51">
<ul class="list-bullet1">
<li><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/31286"><span class="author-a-z122zz84zlz71zdvz68z7k1z83z5u0z80zb">Include bridge configuration into </span></a><span class="author-a-z122zz84zlz71zdvz68z7k1z83z5u0z80zb url"><a href="about:preferences" rel="noreferrer noopener">about:preferences</a></span><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/31286"><span class="author-a-z122zz84zlz71zdvz68z7k1z83z5u0z80zb"> - tpo/applications/tor-browser</span><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z"> </span><span class="author-a-z122zz84zlz71zdvz68z7k1z83z5u0z80zb">#31286</span></a></li>
<li><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/26345"><span class="author-a-z122zz84zlz71zdvz68z7k1z83z5u0z80zb">Disable tracking protection UI in FF67-esr - tpo/applications/tor-browser</span><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z"> </span><span class="author-a-z122zz84zlz71zdvz68z7k1z83z5u0z80zb">#26345</span></a></li>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40321"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">AV1 playback doesn't work on Windows #40321</span></a></li>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/22654"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">Firefox icon is shown for Tor Browser on Windows 10 start menu #22654</span></a></li>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40227"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">Prep 10.5a10 (Windows) #40227</span></a></li>
</ul>
<h2 class="ace-line" id="magicdomid57"><span class="author-a-z122z2z69zz77zuz90zyz72zbz90zcnz73zsz67z4 b"><b>Get</b></span><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z b"><b>T</b></span><span class="author-a-z122z2z69zz77zuz90zyz72zbz90zcnz73zsz67z4 b"><b>or</b></span></h2>
<div class="ace-line" id="magicdomid58"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">GetTor is a tool that allows users </span><span class="author-a-lz82z0z90zm6k8z69zbz78zz83zib9f">to download Tor Browser </span><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">in places where </span><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z url"><a href="https://torproject.org" rel="noreferrer noopener">https://torproject.org</a></span><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z"> is censored</span><span class="author-a-lz82z0z90zm6k8z69zbz78zz83zib9f">.</span><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z"> GetTor responds </span><span class="author-a-lz82z0z90zm6k8z69zbz78zz83zib9f">to emails from users </span><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">with the files</span><span class="author-a-lz82z0z90zm6k8z69zbz78zz83zib9f"> they need to install Tor Browser</span><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">. The Bug Smash Fund helped us to make sure <a href="https://gitlab.torproject.org/tpo/anti-censorship/gettor-project/trac/-/issues/34058">GetTor logs are scrubbed of personal info</a> </span><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z"> (#34058).</span></div>
</div>
</div>
</div>
</div>
<h2 class="ace-line" id="magicdomid60"><span class="author-a-z72z7njz72zz66zkcz89zz83zz90z1tz78z1o b"><b>Network Health</b></span></h2>
<div class="ace-line" id="magicdomid61"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">Over the last month, overload bugs on the directory authorities have caused v3 onion services to become unreliable. The Bug Smash Fund was critical here--it allowed us to pivot from other work to address these issues.</span></div>
<div class="ace-line"> </div>
<div class="ace-line" id="magicdomid63">
<ul class="list-bullet1">
<li><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/40265"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">Rebuild fallbackdir list January 2021 #40265</span></a></li>
<li><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z"><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/40253">Extend the DoS subsystem to block addrs that connect too often too #40253</a> </span></li>
<li><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/40245"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">Assist dir auths with vote visibility #40245</span></a></li>
<li><a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/2667"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">Exits should block reentry into the tor network #2667</span></a></li>
</ul>
</div>
<h2 class="ace-line" id="magicdomid102"><span class="author-a-z122z2z69zz77zuz90zyz72zbz90zcnz73zsz67z4 b"><b>Tor Network Status</b></span></h2>
<div class="ace-line" id="magicdomid103"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">We were in need of a clear, easy-to-read place to share updates on the status of the Tor network when th</span><span class="author-a-lz82z0z90zm6k8z69zbz78zz83zib9f">e</span><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">re have been disruptions. The Bug Smash Fund allowed us to set up </span><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z url"><a href="https://status.torproject.org" rel="noreferrer noopener">status.torproject.org</a></span><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">. Let us know what you think! <a href="https://gitlab.torproject.org/tpo/tpa/schleuder/-/issues/40002">We also fixed Schleuder</a>, a tool we use to communicate with encrypted email to/from <a href="mailto:network-team-security@torproject.org">network-team-security@torproject.org</a> </span><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z"> (#40002).</span></div>
<div class="ace-line" id="magicdomid104"> </div>
<div class="ace-line" id="magicdomid105"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">Thank you to everybody who made a contribution to the Bug Smash Fund. This work is critical in helping us to provide safer tools for millions of people around the world exercising their human rights to privacy and freedom online.</span></div>
<div class="ace-line" id="magicdomid106"> </div>
<div class="ace-line" id="magicdomid107"><span class="author-a-z86zz74zz87zz68zl1cz87zjhxen4z86zz77z">If you’d like to make a contribution to the Bug Smash Fund, you can do so by making a gift at <a href="https://donate.torproject.org">donate.torproject.org</a>: just add “Bug Smash Fund” into the comment field, and we’ll make sure it’s directed to the right place.</span></div>

---
_comments:
<a id="comment-291146"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291146" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 12, 2021</p>
    </div>

    <a href="#comment-291146" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291146" class="permalink" rel="bookmark" hreflang="en">status.torproject.org is…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>status.torproject.org is cool, much easier to understand than consensus-health.torproject.org</p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-291166"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291166" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 16, 2021</p>
    </div>

    <a href="#comment-291166" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291166" class="permalink" rel="bookmark" hreflang="en">Many thanks to everyone at…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Many thanks to everyone at Tor Project who help do the essential work of catching and fixing bugs, network issues, etc!</p>

<p>All the most powerful entities in the world hate everything which truly empowers ordinary citizens to exert some control over their own data destiny.  Consequently, right from the beginning, Tor Project has been continually buffetted by an unending sequences of technical, legal and political threats.  And the Great Lockdown which began early in 2020 seems to have been unusually challenging for TP and all small nonprofits.  But I am cautiously optimistic that 2021 will see things improve for our side.</p>

<p>I hope all readers of the blog will join me in contributing whenever and to what extent we are able.  Let's make Tor a user-supported NGO beholden to no government or megacorporate agenda!</p>

<p>P.S. A new site from ACLU-WA is at coveillance.org.  Check out the Acyclica devices and the NSA secret room featured in the walking tour!  Uhm.... Shouldn;t TP belong to their tech coalition?</p>
</div>

  </div>

</article>
<!-- Comment END -->

<div class="indented"><a id="comment-291168"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291168" class="contextual-region comment js-comment by-node-author" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  alsmith

  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Al Smith</div>
          </div>
</article>

    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/194" lang="">Al Smith</a> said:</p>
      <p class="date-time">February 16, 2021</p>
    </div>

              <p class="parent visually-hidden">In reply to <a href="#comment-291166" class="permalink" rel="bookmark" hreflang="en">Many thanks to everyone at…</a> by <span lang="">Anonymous (not verified)</span></p>

    <a href="#comment-291168" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291168" class="permalink" rel="bookmark" hreflang="en">Thanks for the note about…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for the note about <a href="https://coveillance.org/" rel="nofollow">https://coveillance.org/</a>, I'll check it out.</p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-291184"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291184" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 17, 2021</p>
    </div>

              <p class="parent visually-hidden">In reply to <a href="#comment-291166" class="permalink" rel="bookmark" hreflang="en">Many thanks to everyone at…</a> by <span lang="">Anonymous (not verified)</span></p>

    <a href="#comment-291184" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291184" class="permalink" rel="bookmark" hreflang="en">Isn&#039;t Tor directly funded by…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Isn't Tor directly funded by the NSA? Wikipedia says so. Its weird how the NSA can't break Tor and yet they are willing to fund its development.</p>
</div>

  </div>

</article>
<!-- Comment END -->

<div class="indented"><a id="comment-291190"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291190" class="contextual-region comment js-comment by-node-author" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  alsmith

  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Al Smith</div>
          </div>
</article>

    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/194" lang="">Al Smith</a> said:</p>
      <p class="date-time">February 18, 2021</p>
    </div>

              <p class="parent visually-hidden">In reply to <a href="#comment-291184" class="permalink" rel="bookmark" hreflang="en">Isn&#039;t Tor directly funded by…</a> by <span lang="">Anonymous (not verified)</span></p>

    <a href="#comment-291190" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291190" class="permalink" rel="bookmark" hreflang="en">Tor is not funded by the NSA…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor is not funded by the NSA. That's FUD.</p>

<p>Our financials are open. You can look at an easy to read list of our funders here: <a href="https://www.torproject.org/about/sponsors/" rel="nofollow">https://www.torproject.org/about/sponsors/</a></p>

<p>Or dig deeper in our tax documents here: <a href="https://www.torproject.org/about/financials" rel="nofollow">https://www.torproject.org/about/financials</a></p>

<p>And read posts that explain these documents here: <a href="https://blog.torproject.org/category/tags/form-990" rel="nofollow">https://blog.torproject.org/category/tags/form-990</a></p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-291193"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291193" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 18, 2021</p>
    </div>

              <p class="parent visually-hidden">In reply to <a href="#comment-291184" class="permalink" rel="bookmark" hreflang="en">Isn&#039;t Tor directly funded by…</a> by <span lang="">Anonymous (not verified)</span></p>

    <a href="#comment-291193" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291193" class="permalink" rel="bookmark" hreflang="en">Where does it say that on…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Where does it say that on Wikipedia? I can't find it.</p>
</div>

  </div>

</article>
<!-- Comment END -->

<div class="indented"><a id="comment-291194"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291194" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 19, 2021</p>
    </div>

              <p class="parent visually-hidden">In reply to <a href="#comment-291193" class="permalink" rel="bookmark" hreflang="en">Where does it say that on…</a> by <span lang="">Anonymous (not verified)</span></p>

    <a href="#comment-291194" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291194" class="permalink" rel="bookmark" hreflang="en">Exactly. [citation needed]</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Exactly. <span class="geshifilter"><code class="php geshifilter-php"><span style="color: #009900;">&#91;</span>citation needed<span style="color: #009900;">&#93;</span></code></span> or someone reverts your revision. Always check the History tab and Talk tab when in doubt.</p>
</div>

  </div>

</article>
<!-- Comment END -->
</div></div></div><a id="comment-291172"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291172" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 17, 2021</p>
    </div>

    <a href="#comment-291172" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291172" class="permalink" rel="bookmark" hreflang="en">The way forward: https:/…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The way forward: <a href="https://bestpractices.coreinfrastructure.org/en/criteria" rel="nofollow">https://bestpractices.coreinfrastructure.org/en/criteria</a></p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-291188"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291188" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 18, 2021</p>
    </div>

    <a href="#comment-291188" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291188" class="permalink" rel="bookmark" hreflang="en">Why are gitlab and…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why are gitlab and anonticket logins subdomains of the domain, `onionize.space`, rather than torproject.org? Who owns and operates `onionize.space`? Why is it a potential MITM? Is there more Tor Project infrastructure on non-TorProject domains than those two login subdomains?</p>
</div>

  </div>

</article>
<!-- Comment END -->

<div class="indented"><a id="comment-291191"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291191" class="contextual-region comment js-comment" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  ahf
  </article>

    <div class="comment-header">
      <p class="comment__submitted">ahf said:</p>
      <p class="date-time">February 18, 2021</p>
    </div>

              <p class="parent visually-hidden">In reply to <a href="#comment-291188" class="permalink" rel="bookmark" hreflang="en">Why are gitlab and…</a> by <span lang="">Anonymous (not verified)</span></p>

    <a href="#comment-291191" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291191" class="permalink" rel="bookmark" hreflang="en">It&#039;s my domain that I use…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It's my domain that I use for highly experimental things I do related to Tor. This site will eventually be moved over to Tor Project infrastructure, but since we are working in a time constrained environment, where the internship ends soon, we wanted to get something setup as quickly as possible so that we could gather feedback from the user community.</p>

<p>The goal for this is to have it moved to torproject.org infrastructure and have an onion service. This is likely to happen in the near'ish future.</p>
</div>

  </div>

</article>
<!-- Comment END -->
</div><a id="comment-291189"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291189" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 18, 2021</p>
    </div>

    <a href="#comment-291189" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291189" class="permalink" rel="bookmark" hreflang="en">I like the new status page,…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I like the new status page, but I do wish <a href="https://status.torproject.org/affected/directory-authorities/" rel="nofollow">the description</a> for "directory authorities" was a tad more informative than "whatever it is that those things do". Made me laugh though!<br />
Anyway, amazing progress. Thank you, Tor Project!</p>
</div>

  </div>

</article>
<!-- Comment END -->

<div class="indented"><a id="comment-291195"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291195" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 19, 2021</p>
    </div>

              <p class="parent visually-hidden">In reply to <a href="#comment-291189" class="permalink" rel="bookmark" hreflang="en">I like the new status page,…</a> by <span lang="">Anonymous (not verified)</span></p>

    <a href="#comment-291195" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291195" class="permalink" rel="bookmark" hreflang="en">Some info that could be…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Some info that could be added there as links to improve it:<br />
<a href="https://support.torproject.org/glossary/directory-authority/" rel="nofollow">https://support.torproject.org/glossary/directory-authority/</a><br />
<a href="https://gitlab.torproject.org/legacy/trac/-/wikis/doc/directory_authority" rel="nofollow">https://gitlab.torproject.org/legacy/trac/-/wikis/doc/directory_authori…</a><br />
<a href="https://consensus-health.torproject.org/" rel="nofollow">https://consensus-health.torproject.org/</a></p>

<p>And this is interesting for its in-depth explanations:<br />
<a href="https://matt.traudt.xyz/posts/Debunking:_OSINT_Analysis_of_the_TOR_Foundation/" rel="nofollow">https://matt.traudt.xyz/posts/Debunking:_OSINT_Analysis_of_the_TOR_Foun…</a></p>
</div>

  </div>

</article>
<!-- Comment END -->
</div>
---
