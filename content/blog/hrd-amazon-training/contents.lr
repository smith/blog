title: Digital security tools for human rights defenders
---
pub_date: 2020-11-11
---
author: nah
---
tags:
digital security
human rights
global south
---
image: /static/images/blog/bertha-nah2.png
---
summary:
Communities that depend on forests for their livelihood and environmentalists who fight to protect forests from exploitation live with digital technology in different ways from residents of cities.
---
_html_body:
<p>At the beginning of the Coronavirus pandemic, there wasn't much known about how people living in rural areas, forests, and near rivers would face this new situation. For people living in the world's cities, digital technologies were part of every decision made when considering how to address the pandemic. Still, communities that depend on forests for their livelihood and environmentalists who fight to protect forests from exploitation live with digital technology in different ways from residents of cities. As a result, the changes to daily life that these communities experienced were different.</p>
<p>It didn't take long for organizations worldwide to warn: loggers, land grabbers, and miners do not quarantine. In fact, <a href="https://g1.globo.com/natureza/noticia/2020/05/08/alertas-de-desmatamento-na-amazonia-crescem-em-abril-mostram-dados-do-inpe.ghtml">according to The National Institute for Space Research</a> (Inpe), logging in the Brazilian Amazon increased 63% in April, a month after the global pandemic had its start in the country. Along with the pandemic, Brazilian Amazonian communities face all kinds of challenges: from access to communications to the struggle for survival - and during the pandemic, a challenge was strengthened: the issues of fake news and secure communication between communities and third-party organizations.</p>
<p>Since July 2020, I've been working with the Tor Project as a <a href="https://berthafoundation.org/bertha-challenge/">Bertha Fellow</a> to strengthen and promote digital security among individuals and organizations in the Amazonian region of Brazil,  where I work with the technological challenges of the people who live there fighting to protect forests: lack of internet access or unstable access; lack of telecommunication signals; limited or no variety in telecommunication service providers; lack of inexpensive devices and little availability to update systems due to poor internet access; fake news against movements and individuals, sometimes coming directly from the government; communications surveillance; little access to secure technologies and devices; little availability of alternative and secure services compared to commercial services, especially to store files and online meetings.</p>
<p>Still, some platforms can strengthen these people's security on the front lines against global warming. Due to the limited communications and internet signal, the people I work with often need to use VPN to access public wifi - one of the biggest challenges on these platforms is the language and availability of these tools for old devices. In other software, such as Veracrypt, the problem is the same since there is no translation and localization to Portuguese. Some other software is only produced for 64-bit computers. Due to limited access to resources, some machines used by organizations are still 32-bit; the same is true for applications that do not support or have severe problems on older or cheaper phones.  Because of these challenges, the people and organizations I work with can often be very creative about exchanging information when there is no access to the internet, and many are using Briar to circumvent this problem.</p>
<p>The use of Tor Browser came easily to them in terms of connectivity and the use of social media or email accounts. It's common sense that the Internet connection can be troubling in a place where there isn't much broadband available, but the needs for privacy and security surpass the challenges. Also, choosing the right app in Apple Store can be confusing due to many other applications using Tor's name to attract people.</p>
<p>Despite the challenges, organizations and individuals understand the need, and are willing to change their habits regarding digital security. Because they know that their safety is also the security of the forests, their territory, and, consequently, their lives.</p>

---
