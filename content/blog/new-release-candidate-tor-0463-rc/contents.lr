title: New release candidate: Tor 0.4.6.3-rc
---
pub_date: 2021-05-10
---
author: nickm
---
tags:
release candidate
---
image: /static/images/blog/tor-logo-blog_33.png
---
summary:

---
_html_body:
<p>There's a new release candidate available for download. If you build Tor from source, you can download the source code for Tor 0.4.6.3-rc from the <a href="https://www.torproject.org/download/tor/">download page</a> on the website. Packages should be available over the coming weeks, with a new Tor Browser release likely next week.</p>
<p>Tor 0.4.6.3-rc is the first release candidate in its series. It fixes a few small bugs from previous versions, and adds a better error message when trying to use (no longer supported) v2 onion services.</p>
<p>Though we anticipate that we'll be doing a bit more clean-up between now and the stable release, we expect that our remaining changes will be fairly simple. There will likely be at least one more release candidate before 0.4.6.x is stable.</p>
<h2>Changes in version 0.4.6.3-rc - 2021-05-10</h2>
<ul>
<li>Major bugfixes (onion service, control port):
<ul>
<li>Make the ADD_ONION command properly configure client authorization. Before this fix, the created onion failed to add the client(s). Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40378">40378</a>; bugfix on 0.4.6.1-alpha.</li>
</ul>
</li>
<li>Minor features (compatibility, Linux seccomp sandbox):
<ul>
<li>Add a workaround to enable the Linux sandbox to work correctly with Glibc 2.33. This version of Glibc has started using the fstatat() system call, which previously our sandbox did not allow. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40382">40382</a>; see the ticket for a discussion of trade-offs.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor features (compilation):
<ul>
<li>Make the autoconf script build correctly with autoconf versions 2.70 and later. Closes part of ticket <a href="https://bugs.torproject.org/tpo/core/tor/40335">40335</a>.</li>
</ul>
</li>
<li>Minor features (geoip data):
<ul>
<li>Update the geoip files to match the IPFire Location Database, as retrieved on 2021/05/07.</li>
</ul>
</li>
<li>Minor features (onion services):
<ul>
<li>Add a warning message when trying to connect to (no longer supported) v2 onion services. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40373">40373</a>.</li>
</ul>
</li>
<li>Minor bugfixes (build, cross-compilation):
<ul>
<li>Allow a custom "ar" for cross-compilation. Our previous build script had used the $AR environment variable in most places, but it missed one. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40369">40369</a>; bugfix on 0.4.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (compiler warnings):
<ul>
<li>Fix an indentation problem that led to a warning from GCC 11.1.1. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40380">40380</a>; bugfix on 0.3.0.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (logging, relay):
<ul>
<li>Emit a warning if an Address is found to be internal and tor can't use it. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40290">40290</a>; bugfix on 0.4.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (onion service, client, memory leak):
<ul>
<li>Fix a bug where an expired cached descriptor could get overwritten with a new one without freeing it, leading to a memory leak. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40356">40356</a>; bugfix on 0.3.5.1-alpha.</li>
</ul>
</li>
</ul>

---
