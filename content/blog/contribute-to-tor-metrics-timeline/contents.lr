title: How to contribute to the Tor metrics timeline
---
pub_date: 2021-03-12
---
author: dcf
---
tags:
metrics
volunteer
---
summary:
The [metrics timeline](https://gitlab.torproject.org/tpo/metrics/timeline) is a database of news and events that may affect [Tor Metrics](https://metrics.torproject.org/) graphs. This post is about how you can contribute to the timeline and help keep it up to date.
---
_html_body:
<p>The <a href="https://gitlab.torproject.org/tpo/metrics/timeline">metrics timeline</a> is a database of news and events that may affect <a href="https://metrics.torproject.org/">Tor Metrics</a> graphs. This post is about how you can contribute to the timeline and help keep it up to date.</p>
<p>A timeline of events helps in interpreting graphs. For example, you may look at a graph and ask, "Why did the number of Tor users in Sri Lanka increase for a week in 2018?"</p>
<p><a href="https://metrics.torproject.org/userstats-relay-country.html?start=2018-02-01&amp;end=2018-04-15&amp;country=lk&amp;events=off"><img style="max-width: 100%; max-height: 18em;" alt="Directly connecting users from Sri Lanka" data-entity-type="file" data-entity-uuid="6f86a804-ad17-4f6f-b578-c086079b4d98" src="/static/images/blog/inline-images/userstats-relay-country-lk-2018-02-01-2018-04-15-off.png" /></a></p>
<p>Checking the timeline, we find that at that time in Sri Lanka there was a block of Facebook and other services. A likely explanation for the increase of users is that people were using Tor to access the blocked services.</p>
<table style="font-size: small; overflow-x: auto;">
<thead>
<tr>
<th style="border: thin solid gray; padding: 0 0.25em;">start date</th>
<th style="border: thin solid gray; padding: 0 0.25em;">end date</th>
<th style="border: thin solid gray; padding: 0 0.25em;">places</th>
<th style="border: thin solid gray; padding: 0 0.25em;">protocols</th>
<th style="border: thin solid gray; padding: 0 0.25em;">description</th>
<th style="border: thin solid gray; padding: 0 0.25em;">links</th>
<th style="border: thin solid gray; padding: 0 0.25em;">?</th>
</tr>
</thead>
<tbody>
<tr>
<td style="border: thin solid gray; padding: 0 0.25em;">2018-03-07</td>
<td style="border: thin solid gray; padding: 0 0.25em;">2018-03-15</td>
<td style="border: thin solid gray; padding: 0 0.25em;">lk</td>
<td style="border: thin solid gray; padding: 0 0.25em;"></td>
<td style="border: thin solid gray; padding: 0 0.25em;">Sri Lanka blocks Facebook, Instagram, WhatsApp, and Viber.</td>
<td style="border: thin solid gray; padding: 0 0.25em;"><a href="https://metrics.torproject.org/userstats-relay-country.html?start=2018-01-01&amp;end=2018-04-01&amp;country=lk">relay graph</a> <a href="https://metrics.torproject.org/userstats-bridge-country.html?start=2018-01-01&amp;end=2018-04-01&amp;country=lk">bridge graph</a> <a href="https://www.reuters.com/article/us-sri-lanka-clashes-socialmedia/sri-lanka-lifts-ban-on-facebook-imposed-after-spasm-of-communal-violence-idUSKCN1GR31R">article</a></td>
<td style="border: thin solid gray; padding: 0 0.25em;"></td>
</tr>
</tbody>
</table>
<p>Or, you may look at the graph of relays' advertised bandwidth and ask, "What accounts for the bump in August 2018?"</p>
<p><a href="https://metrics.torproject.org/bandwidth.html?start=2019-07-01&amp;end=2019-09-01"><img style="max-width: 100%; max-height: 18em;" alt="Total relay bandwidth" data-entity-type="file" data-entity-uuid="e6c7d999-380d-4f99-bd54-5b61dbf704a2" src="/static/images/blog/inline-images/bandwidth-2019-07-01-2019-09-01.png" /></a></p>
<p>The timeline has an explanation for this, too. It was the result of a temporary experiment to test the accuracy of relays' bandwidth reports:</p>
<table style="font-size: small; overflow-x: auto;">
<thead>
<tr>
<th style="border: thin solid gray; padding: 0 0.25em;">start date</th>
<th style="border: thin solid gray; padding: 0 0.25em;">end date</th>
<th style="border: thin solid gray; padding: 0 0.25em;">places</th>
<th style="border: thin solid gray; padding: 0 0.25em;">protocols</th>
<th style="border: thin solid gray; padding: 0 0.25em;">description</th>
<th style="border: thin solid gray; padding: 0 0.25em;">links</th>
<th style="border: thin solid gray; padding: 0 0.25em;">?</th>
</tr>
</thead>
<tbody>
<tr>
<td style="border: thin solid gray; padding: 0 0.25em;">2019-08-06</td>
<td style="border: thin solid gray; padding: 0 0.25em;">2019-08-09 01:00:00</td>
<td style="border: thin solid gray; padding: 0 0.25em;"></td>
<td style="border: thin solid gray; padding: 0 0.25em;"></td>
<td style="border: thin solid gray; padding: 0 0.25em;">Experiment to test the accuracy of relays' advertised bandwidth estimation.</td>
<td style="border: thin solid gray; padding: 0 0.25em;"><a href="https://lists.torproject.org/pipermail/tor-relays/2019-July/017535.html">description of experiment</a> <a href="https://lists.torproject.org/pipermail/tor-relays/2019-August/017599.html">post announcing start</a> <a href="https://lists.torproject.org/pipermail/tor-relays/2019-August/017617.html">post announcing end</a> <a href="https://metrics.torproject.org/bandwidth.html?start=2019-07-01&amp;end=2019-09-01">advertised bandwidth graph</a> <a href="https://metrics.torproject.org/bandwidth-flags.html?start=2019-07-01&amp;end=2019-09-01">relay flags graph</a></td>
<td style="border: thin solid gray; padding: 0 0.25em;"></td>
</tr>
</tbody>
</table>
<p>The metrics timeline is useful but incomplete—for example, it tends to only include events that make international news. Some past events have a start date but are missing an end date. And some events mark unusual graph features, but do not have an explanation. You can help the Tor Project and people trying to understand use of the Tor network by contributing your knowledge to the metrics timeline.</p>
<h2>Data format</h2>
<p>The timeline is a <a href="https://gitlab.torproject.org/tpo/metrics/timeline/-/blob/master/README.md">text file called README.md</a> in the Tor Project's GitLab. Entries are stored in a table under the <code>## Timeline</code> heading. The table has seven columns:</p>
<pre><code>|start date|end date|places|protocols|description|links|?  |
|----------|--------|------|---------|-----------|-----|---|
</code></pre><dl>
<dt>start date</dt>
<dd>The date, in the format <code>2020-12-31</code>, when the event began (for long-term events) or occurred (for instantaneous events).</dd>
<dt>end date</dt>
<dd>The date when a long-term event ended. This can be blank for instantaneous events, or the special value <code>ongoing</code> for an event that has started but not ended yet.</dd>
<dt>places</dt>
<dd>A list of <a href="https://en.wikipedia.org/wiki/ISO_3166-1#Current_codes">country codes</a>, separated by spaces. This field may be blank for events that are not limited to a specific geographic region.</dd>
<dt>protocols</dt>
<dd>Some events only affect one or a few of the many protocols used by Tor. For example, this field might contain <code>obfs4</code>, <code>meek</code>, or <code>snowflake</code> if the event affects only one <a href="https://tb-manual.torproject.org/circumvention/">pluggable transport</a>; <code>bridge</code> if <a href="https://tb-manual.torproject.org/bridges/">bridges</a> but not relays are affected; or <code>onion</code> if the event involves <a href="https://tb-manual.torproject.org/onion-services/">onion services</a>. Leave the field blank if the event is not specific to a protocol.</dd>
<dt>description</dt>
<dd>A freeform text description of the event.</dd>
<dt>links</dt>
<dd>A list of links to references or other evidence, in <a href="https://en.wikipedia.org/wiki/Markdown">Markdown</a> format: <code>[link label](https://example.com)</code>.</dd>
<dt>?</dt>
<dd>This field indicates whether the event is adequately explained. An <code>X</code> in the <strong>?</strong> field means that there is an unusual feature on the graph, but we do not know what caused it. The field is blank when a likely cause is known.</dd>
</dl>
<p>Here are two examples. The first one had to do with Iran (country code <code>ir</code>) and is considered adequately explained, with links to two reports. The second one affected only the meek pluggable transport in Brazil (country code <code>br</code>). Its cause is unknown, so it only has a link to a graph, and an <code>X</code> in the <strong>?</strong> field.</p>
<pre><code>|2019-11-16|2019-11-23|ir||Internet blackout in Iran.|[report](https://ooni.org/post/2019-iran-internet-blackout/) [NTC thread](https://ntc.party/t/a-new-kind-of-censoship-in-iran/237)||
|2017-06-07|2017-12-14|br|meek|Sustained increase of meek users in Brazil, similar to the one that took place between 2016-07-21 and 2017-03-03.|[graph](https://metrics.torproject.org/userstats-bridge-combined.html?start=2016-06-01&amp;end=2018-01-15&amp;country=br)|X|
</code></pre><p>Here are some specific ways you can help:</p>
<ul>
<li>Find an event with <code>ongoing</code> in the <strong>end date</strong> field, and see if it really is still ongoing. If it is not, fill in the end date.</li>
<li>Think of Tor-related or Internet-related events that have happened in the country you live in, and make sure there is an entry for each of them.</li>
<li>Find an event with <code>X</code> in the <strong>?</strong> field and search for references or online discussion that may explain it.</li>
<li>Search the graphs for unusual features and add entries for them, with an <code>X</code> in the <strong>?</strong> field.</li>
</ul>
<h2>How to contribute</h2>
<p>To add an entry to the metrics timeline, or edit an existing entry, you will need to create a Tor GitLab account, edit the file, and make a <a href="https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#new-merge-request-by-adding-editing-and-uploading-a-file">merge request</a>.</p>
<p>Merge requests are the most convenient way for us to accept changes, but if you prefer not to create an account, or if you have trouble with the merge request, we still want you to be able to contribute. <a href="https://support.torproject.org/get-in-touch/">Contact support</a> and we will try to help. An alternative to making a merge request is to <a href="https://gitlab.torproject.org/tpo/metrics/timeline/-/issues/new">open a new issue</a> with all the necessary information.</p>
<ol>
<li>
<p>Create a GitLab account at <a href="https://gitlab.onionize.space/">https://gitlab.onionize.space/</a>. Where it says "Please explain why you want to collaborate with the Tor community," you can enter "I want to help with the metrics timeline." Then wait until the new account is approved.</p>
</li>
<li>
<p>Log in with your account and go to <a href="https://gitlab.torproject.org/tpo/metrics/timeline/-/blob/master/README.md">README.md</a> file in the metrics timeline repository.</p>
</li>
<li>
<p>Click the blue "Edit" button. The first time you do this, you will see a message:</p>
<blockquote><p>You're not allowed to edit files in this project directly. Please fork this project, make your changes there, and submit a merge request.</p>
</blockquote>
<p>Click the "Fork" button.</p>
</li>
<li>
<p>Now you may edit the text file. You may want to change the "No wrap" setting to "Soft wrap" to make it easier to read. Scroll down, below the the <code>## Timeline</code> heading, and make your change. In the "Commit message" box, enter a one-sentence summary of the change, like "March 2021 outage in &lt;country&gt;." Click the green "Commit changes" button.</p>
</li>
<li>
<p>At the "New Merge Request" page, leave the default options and click the green "Submit merge request" button.</p>
</li>
</ol>
<p>Now the merge request is created. You will get a notification by email when it is approved, or if the maintainers have questions.</p>
<h2>Step-by-step example</h2>
<p>Let's go step by step through the process of finding something to improve, making the change, and starting a merge request. We will look for an ongoing event without an end date and find out when it actually ended.</p>
<p>We will need a Tor GitLab account, so we go to <a href="https://gitlab.onionize.space/">https://gitlab.onionize.space/</a> and request an account.</p>
<p><a href="https://gitlab.onionize.space/"><img style="max-width: 100%; max-height: 18em;" alt="Account request form" data-entity-type="file" data-entity-uuid="241fc43e-99f9-4ea5-93da-29c58cf52ca8" src="/static/images/blog/inline-images/metrics-timeline-1-account.png" /></a></p>
<p>After the account is created, we can go to <a href="https://gitlab.torproject.org/">https://gitlab.torproject.org/</a> and log in. We go to <a href="https://gitlab.torproject.org/tpo/metrics/timeline/-/blob/master/README.md">README.md</a> in the metrics timeline repository and do a Ctrl+F search for "ongoing". This finds a shutdown event in Ethiopia in mid-2020, which was ongoing at the time the event was added:</p>
<table style="font-size: small; overflow-x: auto;">
<thead>
<tr>
<th style="border: thin solid gray; padding: 0 0.25em;">start date</th>
<th style="border: thin solid gray; padding: 0 0.25em;">end date</th>
<th style="border: thin solid gray; padding: 0 0.25em;">places</th>
<th style="border: thin solid gray; padding: 0 0.25em;">protocols</th>
<th style="border: thin solid gray; padding: 0 0.25em;">description</th>
<th style="border: thin solid gray; padding: 0 0.25em;">links</th>
<th style="border: thin solid gray; padding: 0 0.25em;">?</th>
</tr>
</thead>
<tbody>
<td style="border: thin solid gray; padding: 0 0.25em;">2020-06-30 05:00:00</td>
<td style="border: thin solid gray; padding: 0 0.25em;">ongoing</td>
<td style="border: thin solid gray; padding: 0 0.25em;">et</td>
<td style="border: thin solid gray; padding: 0 0.25em;"></td>
<td style="border: thin solid gray; padding: 0 0.25em;">Internet shutdown in Ethiopia.</td>
<td style="border: thin solid gray; padding: 0 0.25em;"><a href="https://edition.cnn.com/2020/06/30/africa/ethiopia-singer-killing-sparks-protest-intl/">article</a> <a href="https://ioda.caida.org/ioda/dashboard#view=inspect&amp;entity=country/ET&amp;lastView=overview&amp;from=1593192460&amp;until=1593797260">IODA</a> <a href="https://metrics.torproject.org/userstats-relay-country.html?start=2020-06-01&amp;end=2020-07-31&amp;country=et&amp;events=off">relay users graph</a></td>
<td style="border: thin solid gray; padding: 0 0.25em;"></td>

</tbody>
</table>
<p>Looking at the <a href="https://metrics.torproject.org/userstats-relay-country.html?start=2020-06-01&amp;end=2020-07-31&amp;country=et&amp;events=off">graph of directly connecting users</a> from that time, the effect of the shutdown on June 30 is obvious, but it looks like the number of users recovers a few weeks later:</p>
<p><a href="https://metrics.torproject.org/userstats-relay-country.html?start=2020-06-01&amp;end=2020-07-31&amp;country=et&amp;events=off"><img style="max-width: 100%; max-height: 18em;" alt="Directly connecting users from Ethiopia" data-entity-type="file" data-entity-uuid="a131513e-f38d-4645-bbe1-aa2d9656d9eb" src="/static/images/blog/inline-images/userstats-relay-country-et-2020-06-01-2020-07-31-off.png" /></a></p>
<p>Then we do a web search for terms like "Ethiopia Internet shutdown 2020". We find <a href="https://qz.com/africa/1884387/ethiopia-internet-is-back-on-but-oromo-tensions-remain/">an article</a> saying that Internet access was partially restored on July 15 and the shutdown ended on July 23, which is consistent with the Tor Metrics graph. We will make three changes to the entry:</p>
<ol>
<li>Change the <strong>end date</strong> field to <code>2020-07-23</code>.</li>
<li>Note the 2020-07-15 partial restoration of access in the <strong>description</strong> field.</li>
<li>Add a link to the article to the <strong>links</strong> field.</li>
</ol>
<p>Still at the <a href="https://gitlab.torproject.org/tpo/metrics/timeline/-/blob/master/README.md">README.md</a> page, we click the blue "Edit" button, then the green "Fork" button.</p>
<p><img style="max-width: 100%; max-height: 18em;" alt="Screenshot of clicking the &quot;Edit&quot; and &quot;Fork&quot; buttons" data-entity-type="file" data-entity-uuid="19074563-bcba-4a4b-a32f-bfd1e2fdd678" src="/static/images/blog/inline-images/metrics-timeline-2-fork.png" /></p>
<p>We scroll down to find the entry we are interested in:</p>
<p><img style="max-width: 100%; max-height: 18em;" alt="Text of the Ethiopia entry, before editing" data-entity-type="file" data-entity-uuid="2ee48ce2-0658-4730-b73d-c18751258c9c" src="/static/images/blog/inline-images/metrics-timeline-3-edit-before.png" /></p>
<p>And then make these changes:</p>
<ol>
<li>Set <strong>end date</strong> to <code>2020-07-23</code>.</li>
<li>Add to <strong>description</strong>: <code>Restrictions were partially lifted 2020-07-15</code>.</li>
<li>Add to <strong>links</strong>: <code>[article](https://qz.com/africa/1884387/ethiopia-internet-is-back-on-but-oromo-tensions-remain/)</code>.</li>
</ol>
<p><img style="max-width: 100%; max-height: 18em;" alt="Text of the Ethiopia entry, after editing" data-entity-type="file" data-entity-uuid="237e1b7f-d3e5-448a-a4cc-e50dceea9be0" src="/static/images/blog/inline-images/metrics-timeline-4-edit-after.png" /></p>
<p>In the "Commit message" box, we enter "June 2020 shutdown in Ethiopia ended in July," then click the green "Commit changes" button.</p>
<p><img style="max-width: 100%; max-height: 18em;" alt="Screenshot of clicking the &quot;Commit changes&quot; button" data-entity-type="file" data-entity-uuid="ff01f1c7-0654-40b9-8957-9d5da80cb4f6" src="/static/images/blog/inline-images/metrics-timeline-5-commit.png" /></p>
<p>We arrive at a page titled "New Merge Request." We leave all the options unchanged and click the green "Submit merge request" button.</p>
<p><img style="max-width: 100%; max-height: 18em;" alt="Screenshot of clicking the &quot;Submit merge request&quot; button" data-entity-type="file" data-entity-uuid="841c1be0-2a62-4541-be8b-808534556806" src="/static/images/blog/inline-images/metrics-timeline-6-submit.png" /></p>
<p>Now the merge request is created and you are done. Just wait for the maintainer to merge the change or leave a comment. You will get an email notification when something in the merge request changes.</p>
<p>You can see the merge request that was created and merged for this example at <a href="https://gitlab.torproject.org/tpo/metrics/timeline/-/merge_requests/2">tpo/metrics/timeline!2</a>.</p>

---
_comments:
<a id="comment-291351"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291351" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">anon (not verified)</span> said:</p>
      <p class="date-time">March 12, 2021</p>
    </div>

    <a href="#comment-291351" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291351" class="permalink" rel="bookmark" hreflang="en">Would be nice if the…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Would be nice if the timeline were overlaid over the metrics graph. I didn't even know the timeline existed. Thanks!</p>
</div>

  </div>

</article>
<!-- Comment END -->

<div class="indented"><a id="comment-291353"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291353" class="contextual-region comment js-comment by-node-author" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  dcf
  </article>

    <div class="comment-header">
      <p class="comment__submitted">dcf said:</p>
      <p class="date-time">March 13, 2021</p>
    </div>

              <p class="parent visually-hidden">In reply to <a href="#comment-291351" class="permalink" rel="bookmark" hreflang="en">Would be nice if the…</a> by <span lang="">anon (not verified)</span></p>

    <a href="#comment-291353" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291353" class="permalink" rel="bookmark" hreflang="en">I agree. In fact, placing…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I agree. In fact, placing annotations directly on graphs was part of the original motivation behind assembling a timeline. For now, information from the timeline appears on the <a href="https://metrics.torproject.org/news.html" rel="nofollow">News page</a> at Tor Metrics, and in a <a href="https://metrics.torproject.org/userstats-relay-country.html?start=2018-03-01&amp;end=2018-03-15&amp;country=lk&amp;events=off" rel="nofollow">"Related events"</a> section located below graphs (scroll down). The "Related events" sections are currently not very targeted to the graph (see how there's a Snowflake event under the Sri Lanka graph). The timeline information on the Tor Metrics site is also a few months out of date at the moment. <a href="https://gitlab.torproject.org/tpo/metrics/timeline/-/issues/4" rel="nofollow">Issue #4</a> is about importing the latest updates.</p>
</div>

  </div>

</article>
<!-- Comment END -->
</div><a id="comment-291358"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291358" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">V0KSSY (not verified)</span> said:</p>
      <p class="date-time">March 15, 2021</p>
    </div>

    <a href="#comment-291358" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291358" class="permalink" rel="bookmark" hreflang="en">I just istalled this…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I just istalled this wonderful thing and i´m loving it in every aspect.<br />
Thank you so much for your work and help.</p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-291359"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291359" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 15, 2021</p>
    </div>

    <a href="#comment-291359" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291359" class="permalink" rel="bookmark" hreflang="en">How do we contribute…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How do we contribute <em>anonymously</em>?</p>
</div>

  </div>

</article>
<!-- Comment END -->

<div class="indented"><a id="comment-291360"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291360" class="contextual-region comment js-comment by-node-author" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  dcf
  </article>

    <div class="comment-header">
      <p class="comment__submitted">dcf said:</p>
      <p class="date-time">March 15, 2021</p>
    </div>

              <p class="parent visually-hidden">In reply to <a href="#comment-291359" class="permalink" rel="bookmark" hreflang="en">How do we contribute…</a> by <span lang="">Anonymous (not verified)</span></p>

    <a href="#comment-291360" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291360" class="permalink" rel="bookmark" hreflang="en">How do we contribute…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><blockquote><p>How do we contribute <em>anonymously</em>?</p></blockquote>

<p>I depends on how much anonymity you need. If pseudonymity is okay, you can create a single-purpose email account and use that to establish a GitLab account.</p>

<p>The metrics timeline repository could possibly use the <a href="https://blog.torproject.org/anonymous-gitlab" rel="nofollow">Anonymous Ticket Portal</a>, but it is not set up that way now. This is because there is no one who is yet willing to take on the responsibility of checking a second source of contributions. So far, I have been the primary maintainer of the metrics timeline, but I am only a volunteer, and I cannot afford to spend much time on it. So it's important, at this point, that contributions be low-friction.</p>
</div>

  </div>

</article>
<!-- Comment END -->

<div class="indented"><a id="comment-291370"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291370" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 16, 2021</p>
    </div>

              <p class="parent visually-hidden">In reply to dcf</p>

    <a href="#comment-291370" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291370" class="permalink" rel="bookmark" hreflang="en">And we don&#039;t have to torify…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>And we don't have to Torify git or configure it for anonymization because, as the post explains, an editor is built into the GitLab website.</p>
</div>

  </div>

</article>
<!-- Comment END -->
</div></div>
---
