title: New Release: Tor Browser 10.5a16
---
pub_date: 2021-06-11
---
author: sysrqb
---
tags:
tor browser
tbb
tbb-10.5
---
image: /static/images/blog/tor-browser_0_43_35.png
---
summary:
Tor Browser 10.5a16 is now available from the Tor Browser download page and also from our distribution directory.
---
_html_body:
<p>Tor Browser 10.5a16 is now available from the <a href="https://www.torproject.org/download/alpha/">Tor Browser download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/10.5a16/">distribution directory</a>.</p>
<p><b>Note:</b> This is an alpha release, an experimental version for users who want to help us test new features. For everyone else, we recommend downloading the <a href="https://blog.torproject.org/new-release-tor-browser-10017">latest stable release</a> instead.</p>
<p>This version updates Firefox to 78.11esr and Fenix to 89.0. In addition, Tor Browser 10.5a16 updates Tor to 0.4.6.4-rc. This version includes important <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2021-24/">security updates</a> to Firefox for Desktop and <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2021-23/">security updates</a> for Android.</p>
<p style="background-color:cornsilk; padding-left:1em; padding-right:1em;"><b>Warning</b>:<br />
Tor Browser Alpha does <em><b>not</b> support <u>version 2 onion services</u></em>. Tor Browser (Stable) will <em><b>stop</b> supporting <u>version 2 onion services</u></em> later <em><b>this year</b></em>. Please see the previously published <a href="https://blog.torproject.org/v2-deprecation-timeline">deprecation timeline</a> regarding Tor version 0.4.6. Migrate your services and update your bookmarks to version 3 onion services as soon as possible.</p>
<p>The full changelog since <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master"> Tor Browser 10.5a15:</a></p>
<ul>
<li>All Platforms
<ul>
<li>Update NoScript to 11.2.8</li>
<li>Update Tor to 0.4.6.4-rc</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40432">Bug 40432</a>: Prevent probing installed applications</li>
</ul>
</li>
<li>Windows + OS X + Linux
<ul>
<li>Update Firefox to 78.11.0esr</li>
<li><a href="https://bugs.torproject.org/tpo/applications/torbutton/40037">Bug 40037</a>: Announce v2 onion service deprecation on about:tor</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40428">Bug 40428</a>: Correct minor Cryptocurrency warning string typo</li>
</ul>
</li>
<li>Android
<ul>
<li>Update Fenix to 89.1.1</li>
<li><a href="https://bugs.torproject.org/tpo/applications/android-components/40055">Bug 40055</a>: Rebase android-componets patches on 75.0.22 for Fenix 89</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40165">Bug 40165</a>: Announce v2 onion service deprecation on about:tor</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40169">Bug 40169</a>: Rebase fenix patches to fenix v89.1.1</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40170">Bug 40170</a>: Error building tor-browser-89.1.1-10.5-1</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40453">Bug 40453</a>: Rebase tor-browser patches to 89.0</li>
</ul>
</li>
<li>Build System
<ul>
<li>All Platforms
<ul>
<li>Update Go to 1.15.12</li>
</ul>
</li>
<li>Android
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40290">Bug 40290</a>: Update components for mozilla89-based Fenix</li>
</ul>
</li>
</ul>
</li>
</ul>

---
