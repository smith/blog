title: New stable security releases: 0.3.5.15, 0.4.4.9, 0.4.5.9, 0.4.6.5
---
pub_date: 2021-06-14
---
author: nickm
---
tags:
stable release
security
---
image: /static/images/blog/tor-logo-blog_36.png
---
summary:

---
_html_body:
<p>After months of work, we have a new stable release series! If you build Tor from source, you can download the source code for 0.4.6.5 on the <a href="https://www.torproject.org/download/tor/">download page</a>. Packages should be available within the next several weeks, with a new Tor Browser around the end of the week.</p>
<p>Because this release includes security fixes, we are also releasing updates for our other supported releases. You can find their source at <a href="https://dist.torproject.org/">https://dist.torproject.org</a>:</p>
<ul>
<li><a href="https://dist.torproject.org/tor-0.3.5.15.tar.gz">0.3.5.15</a> <a href="https://dist.torproject.org/tor-0.3.5.15.tar.gz.asc">(gpg signature)</a> <a href="https://gitweb.torproject.org/tor.git/tree/ChangeLog?h=tor-0.3.5.15">(ChangeLog)</a></li>
<li><a href="https://dist.torproject.org/tor-0.4.4.9.tar.gz">0.4.4.9</a> <a href="https://dist.torproject.org/tor-0.4.4.9.tar.gz.asc">(gpg signature)</a> <a href="https://gitweb.torproject.org/tor.git/tree/ChangeLog?h=tor-0.4.4.9">(ChangeLog)</a> [Note that 0.4.4.9 hits end-of-life tomorrow; this is the last supported 0.4.4.9 release.]</li>
<li><a href="https://dist.torproject.org/tor-0.4.5.9.tar.gz">0.4.5.9</a> <a href="https://dist.torproject.org/tor-0.4.5.9.tar.gz.asc">(gpg signature)</a> <a href="https://gitweb.torproject.org/tor.git/tree/ChangeLog?h=tor-0.4.5.9">(ChangeLog)</a></li>
</ul>
<p>Tor 0.4.6.5 is the first stable release in its series. The 0.4.6.x series includes numerous features and bugfixes, including a significant improvement to our circuit timeout algorithm that should improve observed client performance, and a way for relays to report when they are overloaded.</p>
<p>This release also includes security fixes for several security issues, including a denial-of-service attack against onion service clients, and another denial-of-service attack against relays. Everybody should upgrade to one of 0.3.5.15, 0.4.4.9, 0.4.5.9, or 0.4.6.5.</p>
<p>Below are the changes since 0.4.5.8. For a list of changes since 0.4.6.4-rc, see the ChangeLog file.</p>
<h2>Changes in version 0.4.6.5 - 2021-06-14</h2>
<ul>
<li>Major bugfixes (security):
<ul>
<li>Don't allow relays to spoof RELAY_END or RELAY_RESOLVED cell on half-closed streams. Previously, clients failed to validate which hop sent these cells: this would allow a relay on a circuit to end a stream that wasn't actually built with it. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40389">40389</a>; bugfix on 0.3.5.1-alpha. This issue is also tracked as TROVE-2021- 003 and CVE-2021-34548.</li>
</ul>
</li>
<li>Major bugfixes (security, defense-in-depth):
<ul>
<li>Detect more failure conditions from the OpenSSL RNG code. Previously, we would detect errors from a missing RNG implementation, but not failures from the RNG code itself. Fortunately, it appears those failures do not happen in practice when Tor is using OpenSSL's default RNG implementation. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40390">40390</a>; bugfix on 0.2.8.1-alpha. This issue is also tracked as TROVE-2021-004. Reported by Jann Horn at Google's Project Zero.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Major bugfixes (security, denial of service):
<ul>
<li>Resist a hashtable-based CPU denial-of-service attack against relays. Previously we used a naive unkeyed hash function to look up circuits in a circuitmux object. An attacker could exploit this to construct circuits with chosen circuit IDs, to create collisions and make the hash table inefficient. Now we use a SipHash construction here instead. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40391">40391</a>; bugfix on 0.2.4.4-alpha. This issue is also tracked as TROVE-2021-005 and CVE-2021-34549. Reported by Jann Horn from Google's Project Zero.</li>
<li>Fix an out-of-bounds memory access in v3 onion service descriptor parsing. An attacker could exploit this bug by crafting an onion service descriptor that would crash any client that tried to visit it. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40392">40392</a>; bugfix on 0.3.0.1-alpha. This issue is also tracked as TROVE-2021-006 and CVE-2021-34550. Reported by Sergei Glazunov from Google's Project Zero.</li>
</ul>
</li>
<li>Major features (control port, onion services):
<ul>
<li>Add controller support for creating version 3 onion services with client authorization. Previously, only v2 onion services could be created with client authorization. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40084">40084</a>. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Major features (directory authority):
<ul>
<li>When voting on a relay with a Sybil-like appearance, add the Sybil flag when clearing out the other flags. This lets a relay operator know why their relay hasn't been included in the consensus. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40255">40255</a>. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Major features (metrics):
<ul>
<li>Relays now report how overloaded they are in their extrainfo documents. This information is controlled with the OverloadStatistics torrc option, and it will be used to improve decisions about the network's load balancing. Implements proposal 328; closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40222">40222</a>.</li>
</ul>
</li>
<li>Major features (relay, denial of service):
<ul>
<li>Add a new DoS subsystem feature to control the rate of client connections for relays. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40253">40253</a>.</li>
</ul>
</li>
<li>Major features (statistics):
<ul>
<li>Relays now publish statistics about the number of v3 onion services and volume of v3 onion service traffic, in the same manner they already do for v2 onions. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/23126">23126</a>.</li>
</ul>
</li>
<li>Major bugfixes (circuit build timeout):
<ul>
<li>Improve the accuracy of our circuit build timeout calculation for 60%, 70%, and 80% build rates for various guard choices. We now use a maximum likelihood estimator for Pareto parameters of the circuit build time distribution, instead of a "right-censored estimator". This causes clients to ignore circuits that never finish building in their timeout calculations. Previously, clients were counting such unfinished circuits as having the highest possible build time value, when in reality these circuits most likely just contain relays that are offline. We also now wait a bit longer to let circuits complete for measurement purposes, lower the minimum possible effective timeout from 1.5 seconds to 10ms, and increase the resolution of the circuit build time histogram from 50ms bin widths to 10ms bin widths. Additionally, we alter our estimate Xm by taking the maximum of the top 10 most common build time values of the 10ms histogram, and compute Xm as the average of these. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40168">40168</a>; bugfix on 0.2.2.14-alpha.</li>
<li>Remove max_time calculation and associated warning from circuit build timeout 'alpha' parameter estimation, as this is no longer needed by our new estimator from 40168. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/34088">34088</a>; bugfix on 0.2.2.9-alpha.</li>
</ul>
</li>
<li>Major bugfixes (signing key):
<ul>
<li>In the tor-gencert utility, give an informative error message if the passphrase given in `--create-identity-key` is too short. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40189">40189</a>; bugfix on 0.2.0.1-alpha. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Minor features (bridge):
<ul>
<li>We now announce the URL to Tor's new bridge status at <a href="https://bridges.torproject.org/">https://bridges.torproject.org/</a> when Tor is configured to run as a bridge relay. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/30477">30477</a>.</li>
</ul>
</li>
<li>Minor features (build system):
<ul>
<li>New "make lsp" command to auto generate the compile_commands.json file used by the ccls server. The "bear" program is needed for this. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40227">40227</a>.</li>
</ul>
</li>
<li>Minor features (client):
<ul>
<li>Clients now check whether their streams are attempting to re-enter the Tor network (i.e. to send Tor traffic over Tor), and close them preemptively if they think exit relays will refuse them for this reason. See ticket <a href="https://bugs.torproject.org/tpo/core/tor/2667">2667</a> for details. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40271">40271</a>.</li>
</ul>
</li>
<li>Minor features (command line):
<ul>
<li>Add long format name "--torrc-file" equivalent to the existing command-line option "-f". Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40324">40324</a>. Patch by Daniel Pinto.</li>
</ul>
</li>
<li>Minor features (command-line interface):
<ul>
<li>Add build informations to `tor --version` in order to ease reproducible builds. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/32102">32102</a>.</li>
<li>When parsing command-line flags that take an optional argument, treat the argument as absent if it would start with a '-' character. Arguments in that form are not intelligible for any of our optional-argument flags. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40223">40223</a>.</li>
<li>Allow a relay operator to list the ed25519 keys on the command line by adding the `rsa` and `ed25519` arguments to the --list-fingerprint flag to show the respective RSA and ed25519 relay fingerprint. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33632">33632</a>. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Minor features (compatibility):
<ul>
<li>Remove an assertion function related to TLS renegotiation. It was used nowhere outside the unit tests, and it was breaking compilation with recent alpha releases of OpenSSL 3.0.0. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40399">40399</a>.</li>
</ul>
</li>
<li>Minor features (control port, stream handling):
<ul>
<li>Add the stream ID to the event line in the ADDRMAP control event. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40249">40249</a>. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Minor features (dormant mode):
<ul>
<li>Add a new 'DormantTimeoutEnabled' option to allow coarse-grained control over whether the client ever becomes dormant from inactivity. Most people won't need this. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40228">40228</a>.</li>
<li>Add a new 'DormantTimeoutEnabled' option for coarse-grained control over whether the client can become dormant from inactivity. Most people won't need this. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40228">40228</a>.</li>
</ul>
</li>
<li>Minor features (geoip data):
<ul>
<li>Update the geoip files to match the IPFire Location Database, as retrieved on 2021/06/10.</li>
</ul>
</li>
<li>Minor features (logging):
<ul>
<li>Edit heartbeat log messages so that more of them begin with the string "Heartbeat: ". Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40322">40322</a>; patch from 'cypherpunks'.</li>
<li>Change the DoS subsystem heartbeat line format to be more clear on what has been detected/rejected, and which option is disabled (if any). Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40308">40308</a>.</li>
<li>In src/core/mainloop/mainloop.c and src/core/mainloop/connection.c, put brackets around IPv6 addresses in log messages. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40232">40232</a>. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Minor features (logging, diagnostic):
<ul>
<li>Log decompression failures at a higher severity level, since they can help provide missing context for other warning messages. We rate-limit these messages, to avoid flooding the logs if they begin to occur frequently. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40175">40175</a>.</li>
</ul>
</li>
<li>Minor features (onion services):
<ul>
<li>Add a warning message when trying to connect to (no longer supported) v2 onion services. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40373">40373</a>.</li>
</ul>
</li>
<li>Minor features (performance, windows):
<ul>
<li>Use SRWLocks to implement locking on Windows. Replaces the "critical section" locking implementation with the faster SRWLocks, available since Windows Vista. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/17927">17927</a>. Patch by Daniel Pinto.</li>
</ul>
</li>
<li>Minor features (protocol, proxy support, defense in depth):
<ul>
<li>Close HAProxy connections if they somehow manage to send us data before we start reading. Closes another case of ticket <a href="https://bugs.torproject.org/tpo/core/tor/40017">40017</a>.</li>
</ul>
</li>
<li>Minor features (tests, portability):
<ul>
<li>Port the hs_build_address.py test script to work with recent versions of python. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40213">40213</a>. Patch from Samanta Navarro.</li>
</ul>
</li>
<li>Minor features (vote document):
<ul>
<li>Add a "stats" line to directory authority votes, to report various statistics that authorities compute about the relays. This will help us diagnose the network better. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40314">40314</a>.</li>
</ul>
</li>
<li>Minor bugfixes (build):
<ul>
<li>The configure script now shows whether or not lzma and zstd have been used, not just if the enable flag was passed in. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40236">40236</a>; bugfix on 0.4.3.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (compatibility):
<ul>
<li>Fix a failure in the test cases when running on the "hppa" architecture, along with a related test that might fail on other architectures in the future. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40274">40274</a>; bugfix on 0.2.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (compilation):
<ul>
<li>Fix a compilation warning about unused functions when building with a libc that lacks the GLOB_ALTDIRFUNC constant. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40354">40354</a>; bugfix on 0.4.5.1-alpha. Patch by Daniel Pinto.</li>
</ul>
</li>
<li>Minor bugfixes (consensus handling):
<ul>
<li>Avoid a set of bugs that could be caused by inconsistently preferring an out-of-date consensus stored in a stale directory cache over a more recent one stored on disk as the latest consensus. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40375">40375</a>; bugfix on 0.3.1.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (control, sandbox):
<ul>
<li>Allow the control command SAVECONF to succeed when the seccomp sandbox is enabled, and make SAVECONF keep only one backup file to simplify implementation. Previously SAVECONF allowed a large number of backup files, which made it incompatible with the sandbox. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40317">40317</a>; bugfix on 0.2.5.4-alpha. Patch by Daniel Pinto.</li>
</ul>
</li>
<li>Minor bugfixes (directory authorities, voting):
<ul>
<li>Add a new consensus method (31) to support any future changes that authorities decide to make to the value of bwweightscale or maxunmeasuredbw. Previously, there was a bug that prevented the authorities from parsing these consensus parameters correctly under most circumstances. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/19011">19011</a>; bugfix on 0.2.2.10-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (ipv6):
<ul>
<li>Allow non-SOCKSPorts to disable IPv4, IPv6, and PreferIPv4. Some rare configurations might break, but in this case you can disable NoIPv4Traffic and NoIPv6Traffic as needed. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/33607">33607</a>; bugfix on 0.4.1.1-alpha. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Minor bugfixes (key generation):
<ul>
<li>Do not require a valid torrc when using the `--keygen` argument to generate a signing key. This allows us to generate keys on systems or users which may not run Tor. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40235">40235</a>; bugfix on 0.2.7.2-alpha. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Minor bugfixes (logging, relay):
<ul>
<li>Emit a warning if an Address is found to be internal and tor can't use it. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40290">40290</a>; bugfix on 0.4.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (metrics port):
<ul>
<li>Fix a bug that made tor try to re-bind() on an already open MetricsPort every 60 seconds. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40370">40370</a>; bugfix on 0.4.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (onion services, logging):
<ul>
<li>Downgrade the severity of a few rendezvous circuit-related warnings from warning to info. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40207">40207</a>; bugfix on 0.3.2.1-alpha. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Minor bugfixes (relay):
<ul>
<li>Reduce the compression level for data streaming from HIGH to LOW. This should reduce the CPU and memory burden for directory caches. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40301">40301</a>; bugfix on 0.3.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (testing, BSD):
<ul>
<li>Fix pattern-matching errors when patterns expand to invalid paths on BSD systems. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40318">40318</a>; bugfix on 0.4.5.1-alpha. Patch by Daniel Pinto.</li>
</ul>
</li>
<li>Code simplification and refactoring:
<ul>
<li>Remove the orconn_ext_or_id_map structure and related functions. (Nothing outside of unit tests used them.) Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33383">33383</a>. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Removed features:
<ul>
<li>Remove unneeded code for parsing private keys in directory documents. This code was only used for client authentication in v2 onion services, which are now unsupported. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40374">40374</a>.</li>
<li>As of this release, Tor no longer supports the old v2 onion services. They were deprecated last July for security, and support will be removed entirely later this year. We strongly encourage everybody to migrate to v3 onion services. For more information, see <a href="https://blog.torproject.org/v2-deprecation-timeline">https://blog.torproject.org/v2-deprecation-timeline</a> . Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40266">40266</a>. (NOTE: We accidentally released an earlier version of the 0.4.6.1-alpha changelog without this entry. Sorry for the confusion!)</li>
</ul>
</li>
<li>Code simplification and refactoring (metrics, DoS):
<ul>
<li>Move the DoS subsystem into the subsys manager, including its configuration options. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40261">40261</a>.</li>
</ul>
</li>
<li>Documentation (manual):
<ul>
<li>Move the ServerTransport* options to the "SERVER OPTIONS" section. Closes issue <a href="https://bugs.torproject.org/tpo/core/tor/40331">40331</a>.</li>
<li>Indicate that the HiddenServiceStatistics option also applies to bridges. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40346">40346</a>.</li>
<li>Move the description of BridgeRecordUsageByCountry to the section "STATISTICS OPTIONS". Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40323">40323</a>.</li>
</ul>
</li>
<li>Removed features (relay):
<ul>
<li>Because DirPorts are only used on authorities, relays no longer advertise them. Similarly, self-testing for DirPorts has been disabled, since an unreachable DirPort is no reason for a relay not to advertise itself. (Configuring a DirPort will still work, for now.) Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40282">40282</a>.</li>
</ul>
</li>
</ul>

---
_comments:
<a id="comment-291963"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291963" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">MomsSpaghetti (not verified)</span> said:</p>
      <p class="date-time">June 14, 2021</p>
    </div>

    <a href="#comment-291963" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291963" class="permalink" rel="bookmark" hreflang="en">Typo alert -&gt;  &quot;New stable…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Typo alert -&gt;  "New stable security releases: 0.3.5.15, 0.4.4.9, 0.4.5.9, 0.4.6.9<br />
by nickm | June 14, 2021"   </p>

<p>^ there is no 0.4.6.9</p>

<p>Other than that new release 0.4.6.5 builds and runs fine at least with latest Windows/MINGW-W64</p>
</div>

  </div>

</article>
<!-- Comment END -->

<div class="indented"><a id="comment-291967"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291967" class="contextual-region comment js-comment by-node-author" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  nickm
  </article>

    <div class="comment-header">
      <p class="comment__submitted">nickm said:</p>
      <p class="date-time">June 14, 2021</p>
    </div>

              <p class="parent visually-hidden">In reply to <a href="#comment-291963" class="permalink" rel="bookmark" hreflang="en">Typo alert -&gt;  &quot;New stable…</a> by <span lang="">MomsSpaghetti (not verified)</span></p>

    <a href="#comment-291967" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291967" class="permalink" rel="bookmark" hreflang="en">Thanks! I&#039;ve edited the…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks! I've edited the title.</p>
</div>

  </div>

</article>
<!-- Comment END -->

<div class="indented"><a id="comment-291969"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291969" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">MomsSpaghetti (not verified)</span> said:</p>
      <p class="date-time">June 14, 2021</p>
    </div>

              <p class="parent visually-hidden">In reply to nickm</p>

    <a href="#comment-291969" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291969" class="permalink" rel="bookmark" hreflang="en">Always a pleasure to read…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Always a pleasure to read your responses, you should have left it like that for a while to make all those ad-infested copy-pasted blogs embarrass themselves. I doubt anyone having IT blogs care about the content of their articles. Nowadays if you don't have a list of original sources to visit you are doomed.</p>

<p>Just a question too generic to pollute gitlab with, do you think we could have an official Go library for Tor soon, anyone working on it? At least with basic functionality would save lots of trouble with cross compiling and IoT devices.</p>
</div>

  </div>

</article>
<!-- Comment END -->

<div class="indented"><a id="comment-291985"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291985" class="contextual-region comment js-comment by-node-author" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  nickm
  </article>

    <div class="comment-header">
      <p class="comment__submitted">nickm said:</p>
      <p class="date-time">June 16, 2021</p>
    </div>

              <p class="parent visually-hidden">In reply to <a href="#comment-291969" class="permalink" rel="bookmark" hreflang="en">Always a pleasure to read…</a> by <span lang="">MomsSpaghetti (not verified)</span></p>

    <a href="#comment-291985" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291985" class="permalink" rel="bookmark" hreflang="en">Rust is more likely;…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Rust is more likely; probably some time later this year.  If it works out then it shouldn't be too hard for somebody to write Go bindings (I'd hope).</p>
</div>

  </div>

</article>
<!-- Comment END -->
</div></div></div><a id="comment-291964"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291964" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 14, 2021</p>
    </div>

    <a href="#comment-291964" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291964" class="permalink" rel="bookmark" hreflang="en">Packages should be available…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><blockquote><p>Packages should be available within the next several weeks</p></blockquote>

<p>May I just ask, how come it needs to take "several weeks" for deb packages to appear for the new tor 0.4.6.9?</p>
</div>

  </div>

</article>
<!-- Comment END -->

<div class="indented"><a id="comment-291968"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291968" class="contextual-region comment js-comment by-node-author" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  nickm
  </article>

    <div class="comment-header">
      <p class="comment__submitted">nickm said:</p>
      <p class="date-time">June 14, 2021</p>
    </div>

              <p class="parent visually-hidden">In reply to <a href="#comment-291964" class="permalink" rel="bookmark" hreflang="en">Packages should be available…</a> by <span lang="">Anonymous (not verified)</span></p>

    <a href="#comment-291968" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291968" class="permalink" rel="bookmark" hreflang="en">It might be sooner. The…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It might be sooner. The issue is that since most binary packages are built by volunteers who do not work for Tor, we can't make commitments about how quickly they will be out.</p>

<p>(Many platforms have additional checks that they need to do as they do their releases: for example, making sure that they don't get the version numbers wrong in their announcements. [embarrassed face emoji here])</p>
</div>

  </div>

</article>
<!-- Comment END -->
</div><a id="comment-291973"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291973" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 15, 2021</p>
    </div>

    <a href="#comment-291973" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291973" class="permalink" rel="bookmark" hreflang="en">&gt; When voting on a relay…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; When voting on a relay with a Sybil-like appearance, add the Sybil flag when clearing out the other flags. This lets a relay operator know why their relay hasn't been included in the consensus. Closes ticket 40255.</p>

<p>Wouldn't that help operators game the voting criteria?</p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-292037"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-292037" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 23, 2021</p>
    </div>

    <a href="#comment-292037" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-292037" class="permalink" rel="bookmark" hreflang="en">Re: bug 40389

Why circ-…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Re: bug 40389</p>

<p>Why circ-&gt;cpath-&gt;prev instead of keep&amp;use cpath_layer?<br />
Why to count as valid injected 'connected','sendme','data' cells?</p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-292040"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-292040" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 23, 2021</p>
    </div>

    <a href="#comment-292040" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-292040" class="permalink" rel="bookmark" hreflang="en">Was there some issue with v0…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Was there some issue with v0.4.6.5 (other than the initial title here) because it still has not appeared on deb.torproject.org?</p>
</div>

  </div>

</article>
<!-- Comment END -->

---
