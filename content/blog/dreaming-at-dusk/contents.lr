title: Dreaming At Dusk
---
pub_date: 2021-05-13
---
author: root
---
image: /static/images/blog/duskNFT.png
---
summary:
Welcome to the onion space. We've been here since 2014, and we grow every day. In a few months, some onions will rot, while others will blossom. Dusk was the first onion; now there are hundreds of thousands. In an entirely different corner of the universe, where smart contracts thrive, Dusk is being auctioned. You can bid for it by interacting with functions living in a chain of blocks.
---
_html_body:
<div class="ace-line" id="magicdomid7">
<p> </p>
<p></p><center><a href="https://foundation.app/torproject/dreaming-at-dusk-35855"><img height="640" src="https://lh3.googleusercontent.com/pw/ACtC-3f3mJiGGjSWpNeMDluzj6G-N1i3FVuW2CtDiKPPS8SOWruhaNxEGhztcAPUmafjkAt2kqpnLXkRVypSdVD2VuIFZQe9H7Jgb3P4w3w2vGH8b1sBNMLetM6_ph3ACUbI1kZoy1-cTPj5QrlNghXCSu54PQ=w1024-h1280-no?authuser=0" width="512" /></a></center>
<p> </p>
<p><span class="author-a-7z82zeaqz67zrz74z2z74zyz90zz73zdz68z0">The first star of a dying galaxy.</span><span class="author-a-z87zg9z81z7z70z8z90zgiz75zz87zz72z7z77zl"> </span><span class="author-a-7z82zeaqz67zrz74z2z74zyz90zz73zdz68z0">Curves taking over while bits dream at dusk.</span></p>
</div>
<div class="ace-line" id="magicdomid8"> </div>
<div class="ace-line" id="magicdomid10"><span class="author-a-7z82zeaqz67zrz74z2z74zyz90zz73zdz68z0">Welcome to the onion space. We've been here since 2004, and we grow every day. In a few months, some onions will rot, while others will blossom.</span></div>
<div class="ace-line" id="magicdomid11"> </div>
<div class="ace-line"> </div>
<div class="ace-line" id="magicdomid12"><a href="http://duskgytldkxiuqc6.onion"><span class="author-a-7z82zeaqz67zrz74z2z74zyz90zz73zdz68z0">Dusk</span></a><span class="author-a-7z82zeaqz67zrz74z2z74zyz90zz73zdz68z0"> was the <a href="https://lists.torproject.org/pipermail/tor-talk/2006-August/001770.html">first onion</a></span><span class="author-a-7z82zeaqz67zrz74z2z74zyz90zz73zdz68z0">; now there are hundreds of thousands.</span></div>
<div class="ace-line"> </div>
<div class="ace-line"> </div>
<div class="ace-line" id="magicdomid15"><span class="author-a-7z82zeaqz67zrz74z2z74zyz90zz73zdz68z0">In an entirely different corner of the universe, where smart contracts thrive, <a href="https://foundation.app/torproject/dreaming-at-dusk-35855">Dusk is being auctioned</a></span><span class="author-a-7z82zeaqz67zrz74z2z74zyz90zz73zdz68z0">. You can bid for it by interacting with functions living in a chain of blocks.</span></div>
<div class="ace-line" id="magicdomid16"> </div>
<div class="ace-line" id="magicdomid17"><span class="author-a-7z82zeaqz67zrz74z2z74zyz90zz73zdz68z0">The Dusk auction will last about 24 hours. It will end on Friday at around 20:00UTC. When it ends, the winning bidder becomes the owner of a generative art piece created </span><span class="author-a-z87zg9z81z7z70z8z90zgiz75zz87zz72z7z77zl">in collaboration with</span><span class="author-a-7z82zeaqz67zrz74z2z74zyz90zz73zdz68z0"> the artist @ixshells which has been derived directly by the Dusk private key. On November,</span><span class="author-a-z87zg9z81z7z70z8z90zgiz75zz87zz72z7z77zl"> the O</span><span class="author-a-7z82zeaqz67zrz74z2z74zyz90zz73zdz68z0">wner</span><span class="author-a-z87zg9z81z7z70z8z90zgiz75zz87zz72z7z77zl">s </span><span class="author-a-7z82zeaqz67zrz74z2z74zyz90zz73zdz68z0">of the NFT also gets the private key of Dusk directly from its owner.</span></div>
<div class="ace-line" id="magicdomid18"> </div>
<div class="ace-line" id="magicdomid22"><span class="author-a-7z82zeaqz67zrz74z2z74zyz90zz73zdz68z0">We know that miners are heavy to the environment. Part of the auction winnings will be donated to </span><span class="author-a-z87zg9z81z7z70z8z90zgiz75zz87zz72z7z77zl">a grassroot organization</span><span class="author-a-7z82zeaqz67zrz74z2z74zyz90zz73zdz68z0"> fighting on the frontlines of the climate crisis</span><span class="author-a-z87zg9z81z7z70z8z90zgiz75zz87zz72z7z77zl">.</span><span class="author-a-7z82zeaqz67zrz74z2z74zyz90zz73zdz68z0"> At the same time, we've been actively monitoring the developments of Ethereum and we believe that their efforts at moving away from PoW is a fight worth fighting and can't come soon enough.</span></div>
<div class="ace-line" id="magicdomid24"> </div>
<div class="ace-line" id="magicdomid142"><span class="author-a-7z82zeaqz67zrz74z2z74zyz90zz73zdz68z0">Welcome to The Auction. Please take your seat :)</span></div>

---
