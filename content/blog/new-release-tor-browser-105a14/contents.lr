title: New Release: Tor Browser 10.5a14
---
pub_date: 2021-04-14
---
author: sysrqb
---
tags:
tor browser
tbb
tbb-10.5
---
image: /static/images/blog/tor-browser_0_43_33.png
---
summary:
Tor Browser 10.5a14 is now available from the Tor Browser Alpha download page and also from our distribution directory.
---
_html_body:
<p>Tor Browser 10.5a14 is now available from the <a href="https://www.torproject.org/download/alpha/">Tor Browser Alpha download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/10.5a14/">distribution directory</a>.</p>
<p><b>Note:</b> This is an alpha release, an experimental version for users who want to help us test new features. For everyone else, we recommend downloading the <a href="https://blog.torproject.org/new-release-tor-browser-10015">latest stable release</a> instead.</p>
<p>This release updates NoScript to 11.2.4 and updates the Snowflake pluggable transport. This release is the first version that is localized in Burmese, as well. Please report issues as comments here, or through the <a href="https://support.torproject.org/misc/bug-or-feedback/">support channels</a></p>
<p><b>Important Note</b>: Tor Browser Alpha versions do <b>not</b> support version 2 onion services. Please see the previously published <a href="https://blog.torproject.org/v2-deprecation-timeline">deprecation timeline</a>.</p>
<p><strong>Note:</strong> Tor Browser 10.5 <a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40089">does not support CentOS 6</a>.</p>
<p>The <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master">full changelog</a> since Tor Browser 10.5a13:</p>
<ul>
<li>All Platforms
<ul>
<li>Update NoScript to 11.2.4</li>
<li>Translations update</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40261">Bug 40261</a>: Bump versions of snowflake and webrtc</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40263">Bug 40263</a>: Update domain front for Snowflake</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40390">Bug 40390</a>: Add Burmese as a new locale</li>
</ul>
</li>
<li>Windows + OS X + Linux
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-launcher/40007">Bug 40007</a>: Update domain fronting config for Moat</li>
</ul>
</li>
</ul>

---
_comments:
<a id="comment-291561"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291561" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 14, 2021</p>
    </div>

    <a href="#comment-291561" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291561" class="permalink" rel="bookmark" hreflang="en">What is the reason for this…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What is the reason for this release two days before the next tor alpha?</p>
</div>

  </div>

</article>
<!-- Comment END -->

<div class="indented"><a id="comment-291566"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291566" class="contextual-region comment js-comment by-node-author" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>

    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">April 15, 2021</p>
    </div>

              <p class="parent visually-hidden">In reply to <a href="#comment-291561" class="permalink" rel="bookmark" hreflang="en">What is the reason for this…</a> by <span lang="">Anonymous (not verified)</span></p>

    <a href="#comment-291566" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291566" class="permalink" rel="bookmark" hreflang="en">So we can receive feedback…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>So we can receive feedback on some changes that will be backported to Tor Browser stable.  The next tor alpha will be included in the Tor Browser Alpha version that we'll publish next week.</p>
</div>

  </div>

</article>
<!-- Comment END -->
</div><a id="comment-291565"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291565" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 14, 2021</p>
    </div>

    <a href="#comment-291565" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291565" class="permalink" rel="bookmark" hreflang="en">This release is the first…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><blockquote><p>This release is the first version that is localized in Burmese</p></blockquote>

<p>Thank you!!!</p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-291580"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291580" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Alex (not verified)</span> said:</p>
      <p class="date-time">April 15, 2021</p>
    </div>

    <a href="#comment-291580" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291580" class="permalink" rel="bookmark" hreflang="en">TOR is not starting. It just…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>TOR is not starting. It just tells me, there is an error. I have tried earlier versions of TOR and I always had the same problem.<br />
Seems, that something is wrong with Firefox. I have no further information for you. I have no idea, what goes wrong.<br />
Can you help me?</p>
</div>

  </div>

</article>
<!-- Comment END -->

<div class="indented"><a id="comment-291581"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291581" class="contextual-region comment js-comment by-node-author" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>

    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">April 15, 2021</p>
    </div>

              <p class="parent visually-hidden">In reply to <a href="#comment-291580" class="permalink" rel="bookmark" hreflang="en">TOR is not starting. It just…</a> by <span lang="">Alex (not verified)</span></p>

    <a href="#comment-291581" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291581" class="permalink" rel="bookmark" hreflang="en">Can you provide the details…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can you provide the details listed under "Feedback Template" on <a href="https://support.torproject.org/misc/bug-or-feedback/" rel="nofollow">https://support.torproject.org/misc/bug-or-feedback/</a>?</p>

<p>If you are using Windows, please include any anti-virus programs you have installed.</p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-291586"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291586" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 16, 2021</p>
    </div>

              <p class="parent visually-hidden">In reply to <a href="#comment-291580" class="permalink" rel="bookmark" hreflang="en">TOR is not starting. It just…</a> by <span lang="">Alex (not verified)</span></p>

    <a href="#comment-291586" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291586" class="permalink" rel="bookmark" hreflang="en">This blog post is for an…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This blog post is for an alpha/experimental version. In the title, the "a" in "10.5a14" means "alpha". Don't use alphas. Use the standard versions. The most recent blog post for a standard version is for <a href="https://blog.torproject.org/new-release-tor-browser-10015" rel="nofollow">10.0.15</a>.</p>

<p>Read the <a href="https://support.torproject.org/" rel="nofollow">support</a> portal and the <a href="https://tb-manual.torproject.org/" rel="nofollow">Tor Browser manual</a>.</p>

<p>What does the error message say?</p>

<p><strong>To sysrqb:</strong><br />
Is there a way to <a href="https://support.torproject.org/tbb/tbb-21/" rel="nofollow">view the tor log</a> in Windows, MacOS, or Android? Platforms other than GNU/Linux? The support question describes Linux only.</p>
</div>

  </div>

</article>
<!-- Comment END -->

<div class="indented"><a id="comment-291593"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291593" class="contextual-region comment js-comment by-node-author" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>

    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">April 16, 2021</p>
    </div>

              <p class="parent visually-hidden">In reply to <a href="#comment-291586" class="permalink" rel="bookmark" hreflang="en">This blog post is for an…</a> by <span lang="">Anonymous (not verified)</span></p>

    <a href="#comment-291593" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291593" class="permalink" rel="bookmark" hreflang="en">The ability to view logs…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The ability to view logs from the Preferences screen is available on Windows and macOS.</p>
</div>

  </div>

</article>
<!-- Comment END -->

<div class="indented"><a id="comment-291597"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291597" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 17, 2021</p>
    </div>

              <p class="parent visually-hidden">In reply to sysrqb</p>

    <a href="#comment-291597" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291597" class="permalink" rel="bookmark" hreflang="en">I meant at the command…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I meant at the command prompt because Alex said it won't open to reach the preferences page. Are there a commands to view the log in OS that are not Linux?</p>
</div>

  </div>

</article>
<!-- Comment END -->
</div></div></div><a id="comment-291585"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291585" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Heather (not verified)</span> said:</p>
      <p class="date-time">April 15, 2021</p>
    </div>

    <a href="#comment-291585" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291585" class="permalink" rel="bookmark" hreflang="en">Is a version for Mac Silicon…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is a version for Mac Silicon in the works?</p>
</div>

  </div>

</article>
<!-- Comment END -->

<div class="indented"><a id="comment-291603"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291603" class="contextual-region comment js-comment by-node-author" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>

    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">April 20, 2021</p>
    </div>

              <p class="parent visually-hidden">In reply to <a href="#comment-291585" class="permalink" rel="bookmark" hreflang="en">Is a version for Mac Silicon…</a> by <span lang="">Heather (not verified)</span></p>

    <a href="#comment-291603" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291603" class="permalink" rel="bookmark" hreflang="en">We&#039;ll inherit that when Tor…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We'll inherit that when Tor Browsers moves onto the next Firefox ESR (version 91).</p>
</div>

  </div>

</article>
<!-- Comment END -->
</div><a id="comment-291600"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291600" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">blather (not verified)</span> said:</p>
      <p class="date-time">April 18, 2021</p>
    </div>

    <a href="#comment-291600" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291600" class="permalink" rel="bookmark" hreflang="en">Tor would not start for me…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor would not start for me either. Used on android. Not remotely techie, still trying to fathom this damn phone with all its preloaded bloatware.<br />
Worked ok initially with everything routed through orbot. Then all just stopped connecting.</p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-291606"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291606" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 20, 2021</p>
    </div>

    <a href="#comment-291606" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291606" class="permalink" rel="bookmark" hreflang="en">https://blog.mozilla.org…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://blog.mozilla.org/security/2021/03/22/firefox-87-trims-http-referrers-by-default-to-protect-user-privacy/" rel="nofollow">https://blog.mozilla.org/security/2021/03/22/firefox-87-trims-http-refe…</a></p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-291733"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291733" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 05, 2021</p>
    </div>

    <a href="#comment-291733" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291733" class="permalink" rel="bookmark" hreflang="en">Translation for SEO:
…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Translation for SEO:<br />
၎င်းသည်ဗမာဘာသာသို့ပြန်ဆိုထားသော Tor Browser ၏ပထမဆုံးမူကွဲဖြစ်သည်။ ဒါကစမ်းသပ်ဗားရှင်းဖြစ်ပါတယ်။ (10.5a14) ဗားရှင်း ၁၀.၅ ကိုရက်သတ္တပတ်အနည်းငယ်အတွင်းတရားဝင်ထုတ်ပြန်လိမ့်မည်။</p>

<p>Tor ဘရောင်ဇာ ကို ဖန်တီးသူသည် Tor Project ။ Tor Project သည် လူ့အခွင့်အရေးနှင့် လွတ်လပ်ရေးခွင့်များကို တိုးတက်စေရန် အမေရိကန် အကျိုးအမြတ်မယူသော ၅၀၁(ဂ)(၃) အဖွဲ့အစည်းတစ်ခု ဖြစ်ပါသည်။ လွတ်လပ်၍ အမည်ဝှက်ကာ အများသုံးနိုင်ပြီး ပုဂ္ဂိုလ်လုံခြုံရေးစောင့်ရှောက်သော နည်းပညာများ အသုံးပြုပါသည်။ ၎င်းနည်းပညာများကို အကန့်အသတ်မရှိစွာ သုံးစွဲနိုင်ခြင်းနှင့် သိပ္ပံဆိုင်ရာနှင့် ပြည်သူများနားလည်နိုင်စေရန် ပံ့ပိုးထောက်ပံ့ပါသည်။</p>

<p>ဆင်ဆာဖြတ်တောက်မှုကိုရှောင်ကြဉ်ပါ။ <a href="https://support.torproject.org/censorship/" rel="nofollow">Tor တံတားများ</a> သုံးပါ။ (<a href="https://tb-manual.torproject.org/circumvention/" rel="nofollow">လက်စွဲစာအုပ်</a>) <a href="https://support.torproject.org/" rel="nofollow">နည်းပညာအထောက်အပံ့ကိုဖတ်ပါ။</a> <a href="https://community.torproject.org/localization/" rel="nofollow">ဘာသာပြန်ရန်ကူညီသည်။</a></p>

<p><a href="https://support.torproject.org/gettor/" rel="nofollow">torproject.org ပိတ်ထားလျှင် Tor ဘရောင်ဇာ ကိုဘယ်လို download လုပ်ရမည်နည်း?</a></p>

<p>#WhatsHappeningInMyanmar</p>
</div>

  </div>

</article>
<!-- Comment END -->

---
