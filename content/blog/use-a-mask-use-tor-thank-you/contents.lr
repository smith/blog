title: 2020 Fundraising Results: Thank You!
---
pub_date: 2021-01-25
---
author: alsmith
---
tags:
fundraising
use a mask use tor
---
image: /static/images/blog/thank-you_2.png
---
summary:
Thank you to everyone who made a donation in 2020! You make it possible to resist the surveillance pandemic. You’ve made it possible for the Tor Project and the tools we support to survive a very difficult time, and to prepare for 2021 with ambitious plans.
---
_html_body:
<p>We believe Tor is strongest when it is used by and supported by as many people as possible. A diverse user base strengthens the anonymity of Tor users, and diverse funding sources ensure we are beholden to our mission—no single financial source. You showed us the reality of this idea over the past year.</p>
<p>It’s no surprise that the Tor Project faced many uncertainties in our funding sources at the beginning of 2020 when the pandemic changed the world. In the spring, we saw a sharp decrease in individual donations. We watched as all of our planned in-person events, during which we previously raised a significant percentage of our funds, were canceled. Foundations also quickly changed their funding plans to respond to the new challenges of the pandemic. The impact of the pandemic and its economic ripple effects were severe enough that we had to make the difficult choice to lay off one third of the Tor Project staff in order to keep Tor steady through uncertainty. </p>
<p>Despite this difficulty, through the summer, you helped us break records with the <a href="https://blog.torproject.org/tors-bug-smash-fund-86k-raised">2020 Bug Smash Fund</a> and helped us pivot as we adapted to online events like the new <a href="https://torproject.org/privchat">PrivChat</a> series and our first all-online <a href="https://youtu.be/IyWyTypRGWQ">State of the Onion</a>.</p>
<p>You also stepped up to resist the surveillance pandemic in a big way during our year-end campaign and not only made up the difference between the losses experienced at the beginning of the pandemic, but also broke another record!</p>
<p>We are pleased to announce that in 2020, despite the cancellations of in-person events and the sharp decrease in individual donations we saw at the beginning of the pandemic, you helped us to raise $913,110 from individuals, more than any calendar year in the Tor Project’s history. You contributed $376,315 of this figure during the end-of-year campaign—this includes the generous $100,000 match by the <a href="https://blog.torproject.org/friends-of-tor-match-2020">Friends of Tor</a>. (That’s a 19% increase over last year’s year-end campaign.)</p>
<p><strong>Thank you to everyone who made a donation in 2020! </strong>You make it possible to resist the surveillance pandemic. You’ve made it possible for the Tor Project and the tools we support to survive a very difficult time, and to <a href="https://blog.torproject.org/tor-in-2021">prepare for 2021 with ambitious plans</a>.</p>
<p>Here are some of the nitty-gritty details about the results of fundraising during the 2020 calendar year and the 2020 year-end fundraising campaign:</p>
<ul>
<li>In the 2020 calendar year, 11,566 people made their first donation to Tor. It is exciting to see so many people understand the importance of privacy online.</li>
<li>In the 2020 calendar year, you donated about $233,019 in <a href="https://donate.torproject.org/cryptocurrency">cryptocurrency</a>. That makes up approximately 26% of total donations from individuals. A huge thank you to the cryptocurrency community!</li>
<li>During the 2020 year-end campaign, 377 new<a href="https://donate.torproject.org/monthly-giving"> Defenders of Privacy</a> signed on to be monthly donors. Sustaining gifts provide the Tor Project with steady, reliable income that is essential to our ability to respond quickly to unexpected challenges and threats.</li>
<li>During the 2020 year-end campaign, 19 donors made contributions of $1,000 or more during the year-end campaign, making them<a href="https://donate.torproject.org/champions-of-privacy"> Champions of Privacy</a>—eight were first-time donors!</li>
<li>During the 2020 year-end campaign, you donated about $58,296 in cryptocurrency, which is $23,000 more than last year!</li>
</ul>
<p>The bottom line is that the work we have planned for this year—continuing user-first development, improving network performance and network health, expanding mobile support, developing Arti, investigating tokens, and working to unblock Tor—is possible with your support.</p>
<p>Thank you.</p>

---
_comments:
<a id="comment-290979"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-290979" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 25, 2021</p>
    </div>

    <a href="#comment-290979" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-290979" class="permalink" rel="bookmark" hreflang="en">Thank you Tor Project…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you Tor Project developers for your continued efforts to protect privacy and uphold human rights.</p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-290983"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-290983" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">0xx2 (not verified)</span> said:</p>
      <p class="date-time">January 26, 2021</p>
    </div>

    <a href="#comment-290983" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-290983" class="permalink" rel="bookmark" hreflang="en">Thank you guys so much,…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you guys so much, always using tails while reporting/researching.</p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-290985"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-290985" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">B (not verified)</span> said:</p>
      <p class="date-time">January 26, 2021</p>
    </div>

    <a href="#comment-290985" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-290985" class="permalink" rel="bookmark" hreflang="en">Hi, when the donations gifts…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi, when the donations gifts will be shipped? I donated for a mask, and I have no new about it<br />
Thanks</p>
</div>

  </div>

</article>
<!-- Comment END -->

<div class="indented"><a id="comment-290988"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-290988" class="contextual-region comment js-comment by-node-author" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  alsmith

  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Al Smith</div>
          </div>
</article>

    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/194" lang="">Al Smith</a> said:</p>
      <p class="date-time">January 26, 2021</p>
    </div>

              <p class="parent visually-hidden">In reply to <a href="#comment-290985" class="permalink" rel="bookmark" hreflang="en">Hi, when the donations gifts…</a> by <span lang="">B (not verified)</span></p>

    <a href="#comment-290988" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-290988" class="permalink" rel="bookmark" hreflang="en">Hi! Your gift should arrive…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi! Your gift should arrive within 6 weeks of donation. We're a small shop so it can take a little longer than large online retailers. If it has been six weeks and you haven't received any news or your swag, please email giving at tor project dot org and we can help. Thanks for supporting Tor.</p>
</div>

  </div>

</article>
<!-- Comment END -->
</div><a id="comment-290991"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-290991" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">RTP (not verified)</span> said:</p>
      <p class="date-time">January 26, 2021</p>
    </div>

    <a href="#comment-290991" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-290991" class="permalink" rel="bookmark" hreflang="en">Big thank you to all the Tor…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Big thank you to all the Tor developers/Staff making privacy for the masses possible in our sometimes scary, ever changing world.</p>

<p>Wherever Human Rights are under attack, privacy becomes the one right able to protect all others.</p>

<p>Thank you. &lt;3</p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-291033"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291033" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">unknown (not verified)</span> said:</p>
      <p class="date-time">February 01, 2021</p>
    </div>

    <a href="#comment-291033" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291033" class="permalink" rel="bookmark" hreflang="en">thaankful to be hidden</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>thaankful to be hidden</p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-291034"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291034" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Tommaso (not verified)</span> said:</p>
      <p class="date-time">February 01, 2021</p>
    </div>

    <a href="#comment-291034" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291034" class="permalink" rel="bookmark" hreflang="en">Tanks</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tanks</p>
</div>

  </div>

</article>
<!-- Comment END -->

---
