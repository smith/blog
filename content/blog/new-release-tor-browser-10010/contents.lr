title: New Release: Tor Browser 10.0.10
---
pub_date: 2021-02-04
---
author: sysrqb
---
tags:
tor browser
tbb
tbb-10.0
---
image: /static/images/blog/10-0@2x_9.png
---
summary:

---
_html_body:
<p>Tor Browser 10.0.10 is now available from the <a href="https://www.torproject.org/download/">Tor Browser download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/10.0.10/">distribution directory</a>.</p>
<p>This version increases the availability of version 3 (v3) onion services. The fix is included in the <a href="https://blog.torproject.org/node/1990">recently released stable tor versions</a>, as well.</p>
<p><a href="https://blog.torproject.org/node/1990" recently="" released="" stable="" tor="" versions="">The full changelog since </a><a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-10.0-desktop">Desktop</a> and <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-10.0-android">Android </a>Tor Browser 10.0.9 is:</p>
<ul>
<li>All Platforms
<ul>
<li>Update NoScript to 11.2</li>
<li>Update HTTPS Everywhere to 2021.1.27</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40224">Bug 40224</a>: Backport Tor patch for v3 onion services</li>
</ul>
</li>
<li>Android
<ul>
<li>Pick up fix for <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1688783">Mozilla's bug 1688783</a></li>
<li>Pick up fix for <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1688017">Mozilla's bug 1688017</a></li>
</ul>
</li>
</ul>

---
_comments:
<a id="comment-291056"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291056" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">NSA (not verified)</span> said:</p>
      <p class="date-time">February 04, 2021</p>
    </div>

    <a href="#comment-291056" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291056" class="permalink" rel="bookmark" hreflang="en">Good job.</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Good job.</p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-291057"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291057" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Ghost Animator (not verified)</span> said:</p>
      <p class="date-time">February 04, 2021</p>
    </div>

    <a href="#comment-291057" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291057" class="permalink" rel="bookmark" hreflang="en">Tor is pretty good. I…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor is pretty good. I noticed that it based itself off the other web browser called Mozzila Firefox. I do agree however that Tor has better security.</p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-291060"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291060" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 04, 2021</p>
    </div>

    <a href="#comment-291060" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291060" class="permalink" rel="bookmark" hreflang="en">Thank you! Availability of…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you! Availability of v3 onion services is vital for operators to transition from v2 and for <a href="https://blog.torproject.org/more-onions-end-of-campaign" rel="nofollow">#MoreOnionsPorfavor</a>.</p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-291062"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291062" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 04, 2021</p>
    </div>

    <a href="#comment-291062" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291062" class="permalink" rel="bookmark" hreflang="en">Make it so when I allow …</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Make it so when I allow [media] in NoScript's blue permission pop-up, it allows media <strong>at the same time for both</strong> the third-party embedded video host and the first-party site that's embedding it. Right now, the [media] pop-up only allows the third-party site, and that doesn't work.</p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-291063"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291063" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 05, 2021</p>
    </div>

    <a href="#comment-291063" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291063" class="permalink" rel="bookmark" hreflang="en">Thank you very much!</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you very much!</p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-291120"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291120" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anon (not verified)</span> said:</p>
      <p class="date-time">February 10, 2021</p>
    </div>

    <a href="#comment-291120" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291120" class="permalink" rel="bookmark" hreflang="en">Browser doesn&#039;t show regular…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Browser isn't working properly and doesn't show regular sites' certificates in Arch Linux. It's a bug related to glibc.</p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-291155"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291155" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">glibc 2.33 (not verified)</span> said:</p>
      <p class="date-time">February 13, 2021</p>
    </div>

    <a href="#comment-291155" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291155" class="permalink" rel="bookmark" hreflang="en">regular TBB doesn&#039;t work…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>regular TBB doesn't work properly with glibc 2.33. i had to switch to alpha to be able to watch videos. images were partially missing and e.g. github was displayed like Page Style &gt; No Style. maybe you warn your users of an update to 2.33.<br />
platform archlinux</p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-291167"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-291167" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 16, 2021</p>
    </div>

    <a href="#comment-291167" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-291167" class="permalink" rel="bookmark" hreflang="en">When will Your browser…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When will Your browser support Wayland? (More secure than X) <a href="https://en.m.wikipedia.org/wiki/Wayland_(display_server_protocol)" rel="nofollow">Wayland wiki</a></p>
</div>

  </div>

</article>
<!-- Comment END -->

---
