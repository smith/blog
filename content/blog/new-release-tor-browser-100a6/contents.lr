title: New Release: Tor Browser 10.0a6
---
pub_date: 2020-08-27
---
author: sysrqb
---
tags:
tor browser
tbb
tbb-10.0
---
image: /static/images/blog/tor-browser_0_43_19.png
---
summary:

---
_html_body:
<p>Tor Browser 10.0a6 is now available from the <a href="https://www.torproject.org/download/alpha/">Tor Browser Alpha download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/10.0a6/">distribution directory</a>.</p>
<p><b>Note:</b> This is an alpha release, an experimental version for users who want to help us test new features. For everyone else, we recommend downloading the <a href="https://blog.torproject.org/new-release-tor-browser-954">latest stable release</a> instead.</p>
<p>We are happy to announce the second alpha for <strong>desktop users</strong> based on Firefox 78 ESR. The Android version is under active development and will be available in the coming weeks.</p>
<p>We <a href="https://blog.torproject.org/new-release-tor-browser-100a5">released the first alpha version</a> based on ESR 78 one week ago.</p>
<p>Tor Browser 10.0a6 ships with Firefox 78.2.0esr, and updates NoScript to 11.0.39. <strong>Please report bugs with steps to reproduce</strong>, either here or on <a href="https://gitlab.torproject.org">Gitlab</a>, or essentially with any other means that would reach us. We are in particular interested in potential <strong>proxy bypasses</strong> which our proxy audit missed.</p>
<p>Also, this release features important <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2020-37/">security updates</a> to Firefox.</p>
<p><strong>Note</strong>: Android Tor Browser is still based on Firefox 68esr. As a result, <strong>Android</strong> and <strong>desktop</strong> packages were built separately, based on tag tbb-10.0a6-build1 and tbb-10.0a6-build2 respectively. The resulting <b>sha256sums-unsigned-build.txt</b> files were merged and signed before publishing.</p>
<p><strong>Note</strong>: We encountered updater issues for all alpha users that have been auto-updating the alpha series for months. We changed the accepted MAR channel ID to <em>torbrowser-torproject-alpha</em> as we are on an alpha channel. The assumption was that enough time passed since we changed it last time to <em>torbrowser-torproject-release,torbrowser-torproject-alpha</em> but it turns out that change did not get applied. <strong>Workaround:</strong> change the torbrowser-torproject-release in your <em>update-settings.ini</em> (in the <a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/wikis/Platform-Installation">Browser's code directory</a>, which depends on you operating system) file to <em>torbrowser-torproject-alpha</em> and the update should get applied successfully. Alternatively, downloading a fresh alpha copy of Tor Browser works as well. Sorry for the inconvenience.</p>
<p>The full <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt">changelog</a> since Tor Browser 10.0a5 is:</p>
<ul>
<li>Windows + OS X + Linux
<ul>
<li>Update Firefox to 78.2.0esr</li>
<li>Update Tor Launcher to 0.2.23
<ul>
<li><a href="https://gitlab.torproject.org/tpo/applications//tor-launcher/-/issues/40002">Bug 40002: </a>After rebasing to 80.0b2 moat is broken</li>
</ul>
</li>
<li>Translations update</li>
<li>Update NoScript to 11.0.39</li>
<li><a href="https://gitlab.torproject.org/tpo/applications/torbutton/-/issues/21601">Bug 21601:</a> Remove unused media.webaudio.enabled pref</li>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40002">Bug 40002:</a> Remove about:pioneer</li>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40082">Bug 40082:</a> Let JavaScript on safest setting handled by NoScript again</li>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40088">Bug 40088:</a> Moat "Submit" button does not work</li>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40090">Bug 40090:</a> Disable v3 add-on blocklist for now</li>
<li>OS X
<ul>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40015">Bug 40015:</a> Tor Browser broken on MacOS 11 Big Sur</li>
</ul>
</li>
<li>Build System
<ul>
<li>Windows + OS X + Linux
<ul>
<li>Bump Go to 1.13.15</li>
</ul>
</li>
<li>Linux
<ul>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40053">Bug 40053:</a> Also create the langpacks tarball for non-release builds</li>
</ul>
</li>
</ul>
</li>
</ul>
</li>
</ul>
<p>The full <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-10.0a6">changelog</a> since (Android) Tor Browser 10.0a4 is:</p>
<ul>
<li>Android
<ul>
<li>Update Firefox to 68.12.0esr</li>
<li>Update HTTPS Everywhere to 2020.08.13</li>
<li>Update NoScript to 11.0.38</li>
<li>Update Tor to 0.4.4.4-rc</li>
</ul>
</li>
</ul>

---
_comments:
<a id="comment-289262"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-289262" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 26, 2020</p>
    </div>

    <a href="#comment-289262" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-289262" class="permalink" rel="bookmark" hreflang="en">Bug 21601: Remove unused…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Bug 21601: Remove unused media.webaudio.enabled pref<br />
Bug 21601: Remove unused media.webaudio.enabled pref [torbutton]</p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-289263"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-289263" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 26, 2020</p>
    </div>

    <a href="#comment-289263" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-289263" class="permalink" rel="bookmark" hreflang="en">Plus:

Bug 34234: Move HTTPS…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Plus:</p>

<p>Bug 34234: Move HTTPS Everywhere build to Debian Buster<br />
<a href="https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/commit/8feea5ace569b0529d6281b98d95340a0414ce91" rel="nofollow">https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/comm…</a></p>

<p>Update HTTPS Everywhere to 2020.08.13<br />
<a href="https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/commit/d3c7af80c77facabb2633927e264f16723ce983b#da975a328e7b2639f5019cd1d23711aa26f16370" rel="nofollow">https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/comm…</a></p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-289267"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-289267" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 26, 2020</p>
    </div>

    <a href="#comment-289267" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-289267" class="permalink" rel="bookmark" hreflang="en">[08-27 06:38:09] Torbutton…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>[08-27 06:38:09] Torbutton NOTE: Exception on control port [Exception... "Component returned failure code: 0x804b000e (NS_ERROR_NET_TIMEOUT) [nsIBinaryInputStream.readBytes]"  nsresult: "0x804b000e (NS_ERROR_NET_TIMEOUT)"  location: "JS frame :: chrome://torbutton/content/torbutton.js :: torbutton_socket_readline :: line 652"  data: no]</p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-289268"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-289268" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 26, 2020</p>
    </div>

    <a href="#comment-289268" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-289268" class="permalink" rel="bookmark" hreflang="en">TypeError: event.target…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>TypeError: event.target.closest is not a function UrlbarInput.jsm:2172:26<br />
    _on_mousedown resource:///modules/UrlbarInput.jsm:2172<br />
    handleEvent resource:///modules/UrlbarInput.jsm:367</p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-289269"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-289269" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 26, 2020</p>
    </div>

    <a href="#comment-289269" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-289269" class="permalink" rel="bookmark" hreflang="en">NS_ERROR_XPC_JAVASCRIPT…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>NS_ERROR_XPC_JAVASCRIPT_ERROR_WITH_DETAILS: [JavaScript Error: "Invalid autocomplete selectedIndex" {file: "resource://gre/actors/AutoCompleteChild.jsm" line: 208}]'[JavaScript Error: "Invalid autocomplete selectedIndex" {file: "resource://gre/actors/AutoCompleteChild.jsm" line: 208}]' when calling method: [nsIAutoCompletePopup::selectedIndex] LoginManagerChild.jsm:174<br />
    observe resource://gre/modules/LoginManagerChild.jsm:174<br />
    handleEvent resource://gre/actors/AutoCompleteChild.jsm:126</p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-289270"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-289270" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 27, 2020</p>
    </div>

    <a href="#comment-289270" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-289270" class="permalink" rel="bookmark" hreflang="en">NS_ERROR_FAILURE: Should…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>NS_ERROR_FAILURE: Should pass a non-null uri URIFixup.jsm:281<br />
    getFixupURIInfo resource://gre/modules/URIFixup.jsm:281<br />
    _getUrlMetaData resource:///modules/UrlbarValueFormatter.jsm:150<br />
    _ensureFormattedHostVisible resource:///modules/UrlbarValueFormatter.jsm:105</p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-289274"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-289274" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">anon (not verified)</span> said:</p>
      <p class="date-time">August 27, 2020</p>
    </div>

    <a href="#comment-289274" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-289274" class="permalink" rel="bookmark" hreflang="en">I just saw devtools…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I just saw <span class="geshifilter"><code class="php geshifilter-php">devtools<span style="color: #339933;">.</span>onboarding<span style="color: #339933;">.</span>telemetry<span style="color: #339933;">.</span>logged</code></span> in my 9.5.4 and 10.0a6 with a value of <span class="geshifilter"><code class="php geshifilter-php"><span style="color: #009900; font-weight: bold;">true</span></code></span>. Is this a bug?</p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-289283"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-289283" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 28, 2020</p>
    </div>

    <a href="#comment-289283" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-289283" class="permalink" rel="bookmark" hreflang="en">[Exception... &quot;Component…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>[Exception... "Component returned failure code: 0x8050000e (NS_ERROR_ILLEGAL_INPUT) [nsIConverterInputStream.readString]"  nsresult: "0x8050000e (NS_ERROR_ILLEGAL_INPUT)"  location: "JS frame :: resource://gre/modules/WebRequestUpload.jsm :: readString :: line 245"  data: no] WebRequestUpload.jsm:245:12<br />
    readString resource://gre/modules/WebRequestUpload.jsm:245<br />
    readAllStrings resource://gre/modules/WebRequestUpload.jsm:277<br />
    next self-hosted:1099<br />
    getParts resource://gre/modules/WebRequestUpload.jsm:296<br />
    next self-hosted:1099<br />
    parseMultiPart resource://gre/modules/WebRequestUpload.jsm:322<br />
    parseFormData resource://gre/modules/WebRequestUpload.jsm:398<br />
    createFormData resource://gre/modules/WebRequestUpload.jsm:439<br />
    createRequestBody resource://gre/modules/WebRequestUpload.jsm:529<br />
    runChannelListener resource://gre/modules/WebRequest.jsm:888<br />
    forEach self-hosted:1174<br />
    runChannelListener resource://gre/modules/WebRequest.jsm:834<br />
    observe resource://gre/modules/WebRequest.jsm:666</p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-289295"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-289295" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 28, 2020</p>
    </div>

    <a href="#comment-289295" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-289295" class="permalink" rel="bookmark" hreflang="en">I have a buglet to report…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I have a buglet to report. If I use Tor Browser to navigate the file system (e.g., if I want to view files in /tmp), the page does not load properly. Instead, it seems to keep stopping and starting to load the page but it never actually shows the directory listing. I ran into this on 10.0a6 on linux64.</p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-289300"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-289300" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="" content="Wishing You Were Here">Wishing You We… (not verified)</span> said:</p>
      <p class="date-time">August 28, 2020</p>
    </div>

    <a href="#comment-289300" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-289300" class="permalink" rel="bookmark" hreflang="en">Tor Browser goes crazy…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor Browser goes crazy trying to load my plain old HTML file which I use as a home page. And another plain text file I tried. Here's what happens. </p>

<p>Activity Monitor on Mac shows an impressive 109% CPU consumption rate for the "Tor BrowserCP file:// Content" process with Tor Browser process around 80-90%. </p>

<p>The Tor Browser window shows the X and Reload icon changing very fast. The tab label text is likewise cycling back and forth as fast as it can. </p>

<p>It seems to have issues with loading local text files. Pages retrieved via http(s) are fine.<br />
__________<br />
Also, has the rqef5a5mebgq46y5.onion address gone away? It doesn't seem to load any more. I preferred that to the less secure domain name option. :-)</p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-289427"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-289427" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 09, 2020</p>
    </div>

    <a href="#comment-289427" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-289427" class="permalink" rel="bookmark" hreflang="en">NS_ERROR_FAILURE: Component…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>NS_ERROR_FAILURE: Component returned failure code: 0x80004005 (NS_ERROR_FAILURE) [nsIWindowsRegKey.create] InstallerPrefs.jsm:140<br />
    _openRegKey resource:///modules/InstallerPrefs.jsm:140<br />
    observe resource:///modules/InstallerPrefs.jsm:80</p>

<p>Stop touching the registry!</p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-289452"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-289452" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 14, 2020</p>
    </div>

    <a href="#comment-289452" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-289452" class="permalink" rel="bookmark" hreflang="en">When I use a bridge, the…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When I use a bridge, the circuit diagram in the <a href="https://tb-manual.torproject.org/static/images/circuit_full.png" rel="nofollow">"site information" menu</a> on the URL bar displays the bridge's IP address. Isn't it dangerous to display a bridge's IP address in the main interface that is frequently visible to shoulder-surfers? You make it necessary to reveal it to reach the New Circuit button. Wouldn't it be safer to display the bridge's hashed fingerprint or a link to open the Tor preferences tab to view details?</p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-289462"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-289462" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 14, 2020</p>
    </div>

    <a href="#comment-289462" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-289462" class="permalink" rel="bookmark" hreflang="en">Sorry, I don&#039;t know which…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Sorry, I don't know which post to write this in.</p>

<p><strong>Dear web team</strong>, update the name "OS X" to "macOS" on the download page.</p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-289464"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-289464" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 15, 2020</p>
    </div>

    <a href="#comment-289464" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-289464" class="permalink" rel="bookmark" hreflang="en">Double click on the video…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Double click on the video will switch it to full screen, does the video site get my real screen resolution?</p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-289466"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-289466" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 15, 2020</p>
    </div>

    <a href="#comment-289466" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-289466" class="permalink" rel="bookmark" hreflang="en">14:48:00.212 NS_ERROR…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>14:48:00.212 NS_ERROR_FAILURE: Component returned failure code: 0x80004005 (NS_ERROR_FAILURE) [nsIWebNavigation.goForward] 4 WebNavigationChild.jsm:57<br />
    goForward resource://gre/actors/WebNavigationChild.jsm:57<br />
    _wrapURIChangeCall resource://gre/actors/WebNavigationChild.jsm:36<br />
    goForward resource://gre/actors/WebNavigationChild.jsm:57<br />
    receiveMessage resource://gre/actors/WebNavigationChild.jsm:20</p>
</div>

  </div>

</article>
<!-- Comment END -->
<a id="comment-289477"></a>

<div class="comment-meta-filler">
<!-- Comment -->

<article id="comment-289477" class="contextual-region comment js-comment by-anonymous" id="comment-">
    <mark class="hidden"></mark>

  <footer class="comment__meta">
    <article class="contextual-region">
  Anonymous
  </article>

    <div class="comment-header">
      <p class="comment__submitted"><span lang="">why? (not verified)</span> said:</p>
      <p class="date-time">September 16, 2020</p>
    </div>

    <a href="#comment-289477" hreflang="en">Permalink</a>
  </footer>

  <div class="content">

      <h3><a href="#comment-289477" class="permalink" rel="bookmark" hreflang="en">Discussion for new alpha…</a></h3>
      <div></div>

            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Discussion for new alpha<br />
<a href="https://blog.torproject.org/new-release-tor-browser-100a7" rel="nofollow">https://blog.torproject.org/new-release-tor-browser-100a7</a><br />
is closed, i try it here:</p>

<p>Why extensions-/storage-directory will be mixed again?<br />
<a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/320" rel="nofollow">https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/320</a>…<br />
Switch back extensions.webextensions.ExtensionStorageIDB.enabled = true</p>

<p>Super idea? No, it was a bad idea and is a bad idea.<br />
For programmers, ...i don't know. For me, for the user mixing is a useless, a not so good thing.<br />
Separate the directory for extensions and storage, clearly laid out, is really better and there is no need to mess up this? I really see no need for this. No offense.</p>
</div>

  </div>

</article>
<!-- Comment END -->

---
