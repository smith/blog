title: Get a TLS certificate for your onion site
---
pub_date: 2021-03-24
---
author: isabela
---
tags:
onion service
---
image: /static/images/blog/harica-blog.png
---
summary:
We are happy to share the news of another important milestone for .onion services! You can now get DV certificates for your v3 onion site using HARICA, a Root CA Operator founded by Academic Network (GUnet), a civil society nonprofit from Greece.
---
_html_body:
<p>We are happy to share the news of another important milestone for .onion services! You can now get DV certificates for your v3 onion site using <a href="https://www.harica.gr/Contact/GetHarica">HARICA</a>, a Root CA Operator founded by <a href="https://www.gunet.gr/en/">Academic Network (GUnet)</a>, a civil society nonprofit from Greece.</p>
<p>Last year we wrote a blog post about the challenges and opportunities for onion services:</p>
<blockquote><p><em>The future of TLS support for onion services is very encouraging. In March of this year, the CA/Browser Forum approved an <a href="https://cabforum.org/wp-content/uploads/CA-Browser-Forum-BR-1.7.0-1.pdf">amendment to the domain validation (DV) TLS certificate baseline requirements</a> which now allows certificate authorities (CAs) to issue certificates containing v3 .onion addresses. This means, in the not-too-distant-future, a CA like Let's Encrypt can issue a certificate for an onion service and Tor Browser will "just work." In addition, for onion services that do not want to rely on certificate authorities, we are exploring alternative designs like Same Origin Onion Certificate for inclusion in Tor Browser.</em></p>
</blockquote>
<p>We are happy that the ‘not-too-distant-future’ was indeed quite close. We hope that more CAs do the same. </p>
<p>Why would an .onion site need a TLS certificate? This is a great question. Especially because .onion services provide pretty much the same protections offered by an HTTPS connection regarding protecting the data in transit from man in the middle attacks and validating that the user is indeed connecting the server the domain in the browser bar is requesting. Onion services do the same thing, so why would an .onion site need a TLS certificate?</p>
<p>Our <a href="https://community.torproject.org/onion-services/advanced/https/">Community portal page about onion services give you a list of reasons why a service admin would need a TLS certificate as part of their implementation</a>. Here are some of them:</p>
<ul>
<li>Websites with complex setups and that are serving HTTP and HTTPS content</li>
<li>To help the user verify that the .onion address is indeed the site you are hosting (this would be a manual check done by the user looking at the cert registration information)</li>
<li>Some services work with protocols, frameworks, and other infrastructure that has HTTPS connection as a requirement</li>
<li>In case your web server and your tor process are in different machines</li>
</ul>
<p>Previously, .onion site administrators who needed a TLS certificate had to either hack other solutions or spend a significant amount of money purchasing an EV certificate. Now with HARICA, acquiring a certificate has become more accessible, but we know that free certificates are ideal and are looking forward to that moment.</p>
<p>We are happy to see <a href="https://www.reddit.com/r/onions/comments/lwaccm/harica_ca_now_supports_issuance_of_dv_onion/">people acquiring certificates for their onions</a>. Remember to do it for a v3 onion address since <a href="https://blog.torproject.org/v2-deprecation-timeline">v2 will be deprecated</a> very soon:</p>
<blockquote><p><em>2. July 15th, 2021<br />
0.4.6.x: Tor will no longer support v2 and support will be removed from the code base.</em></p>
</blockquote>
<p>If you would like to give it a try, here is a <a href="https://kushaldas.in/posts/get-a-tls-certificate-for-your-onion-service.html">great tutorial by Kushal</a> from the Tor community.<br />
 </p>

---
