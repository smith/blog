# -*- coding: utf-8 -*-
import datetime

from lektor.pluginsystem import Plugin


class TodayPlugin(Plugin):
    name = 'Today'
    description = u'Add your description here.'

    def on_setup_env(self, **extra):
        self.env.jinja_env.globals['today'] = datetime.date.today()
